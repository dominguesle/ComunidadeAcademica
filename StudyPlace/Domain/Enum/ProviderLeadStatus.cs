﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Enum
{
    public enum ProviderLeadStatus
    {
        [Display(Name = "Não Especificado")]
        NaoEspecificado,

        [Display(Name = "Em aberto")]
        EmAberto,

        [Display(Name = "Contato Realizado")]
        ContatoRealizado,

        [Display(Name = "Negócio Fechado")]
        NegocioFechado,

        [Display(Name = "Salvo")]
        Salvo
    }
}
