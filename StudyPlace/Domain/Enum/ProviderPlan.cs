﻿namespace Domain.Enum
{
    public enum ProviderPlan
    {
        None, Basic, Intermediate, Pro
    }
}
