﻿using Domain.Models;
using Domain.Repository;

namespace Domain.Interfaces.Repository
{
    public interface ICompanyRepository : IBaseRepository<Company>
    {        
    }
}
