﻿using System;
using System.Collections.Generic;

namespace Domain.Services
{
    public interface IBaseService<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity entity);
        void Remove(TEntity entity);
        void Update(TEntity entity);

        TEntity GetById(object id);
        IEnumerable<TEntity> GetAll();
    }
}
