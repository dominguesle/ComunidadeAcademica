﻿using Domain.Models;
using Domain.Services;
using System.Collections.Generic;

namespace Domain.Interfaces.Services
{
    public interface ICompanyService : IBaseService<Company>
    {
        Company GetByUserId(string userId);
        List<Company> GetAllOrderByDataDescending();
    }
}
