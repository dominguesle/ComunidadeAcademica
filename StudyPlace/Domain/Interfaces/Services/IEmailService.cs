﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Domain.Services
{
    public interface IEmailService
    {
        Task DeliverAsync(IdentityMessage message);
    }
}
