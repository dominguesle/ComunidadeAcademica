﻿using Domain.Models;
using System;
using System.Collections.Generic;

namespace Domain.Services
{
    public interface IGraduatedInService : IDisposable
    {
        List<GraduatedIn> GetById(List<string> idsList);
        List<GraduatedIn> GetAll();
    }
}
