﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Services
{
    public interface ILanguageService : IDisposable
    {
        List<Language> GetById(List<string> idsList);
        List<Language> GetAll();
    }
}
