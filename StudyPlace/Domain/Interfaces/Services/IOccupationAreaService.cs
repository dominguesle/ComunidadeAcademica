﻿using Domain.Models;
using System;
using System.Collections.Generic;

namespace Domain.Services
{
    public interface IOccupationAreaService : IDisposable
    {
        List<OccupationArea> GetById(List<string> idsList);
        List<OccupationArea> GetAll();
    }
}
