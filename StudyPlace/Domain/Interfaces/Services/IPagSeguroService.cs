﻿using Uol.PagSeguro.Domain;

namespace Domain.Services
{
    public interface IPagSeguroService
    {
        Transaction ProcessNotification(string notificationCode, bool preApproval);
        string CaptureTransaction(PaymentRequest payment);
        string GenerateTransactionCode(PaymentRequest payment);
    }
}