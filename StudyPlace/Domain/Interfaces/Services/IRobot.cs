﻿using System.Threading.Tasks;

namespace Domain.Services
{
    public interface IRobot
    {
        Task Run();
    }
}
