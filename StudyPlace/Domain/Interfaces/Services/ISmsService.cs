﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Domain.Services
{
    public interface ISmsService
    {
        Task DeliverAsync(IdentityMessage message);
    }
}
