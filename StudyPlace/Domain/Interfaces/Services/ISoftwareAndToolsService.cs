﻿using Domain.Models;
using System;
using System.Collections.Generic;

namespace Domain.Services
{
    public interface ISoftwareAndToolsService : IDisposable
    {
        List<SoftwareAndTool> GetById(List<string> idsList);
        List<SoftwareAndTool> GetAll();
    }
}
