namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Street = c.String(nullable: false, maxLength: 8000, unicode: false),
                        Number = c.String(maxLength: 8000, unicode: false),
                        Complement = c.String(maxLength: 8000, unicode: false),
                        CEP = c.String(maxLength: 8000, unicode: false),
                        Neighborhood = c.String(maxLength: 8000, unicode: false),
                        City = c.String(nullable: false, maxLength: 8000, unicode: false),
                        State = c.String(nullable: false, maxLength: 8000, unicode: false),
                        FilialAddress = c.Boolean(nullable: false),
                        Company_Id = c.String(maxLength: 128, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Company_Id)
                .Index(t => t.Company_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, unicode: false),
                        FullName = c.String(nullable: false, maxLength: 8000, unicode: false),
                        CreatedDateTime = c.DateTime(nullable: false),
                        ImageUrl = c.String(maxLength: 8000, unicode: false),
                        Email = c.String(maxLength: 256, unicode: false),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(maxLength: 8000, unicode: false),
                        SecurityStamp = c.String(maxLength: 8000, unicode: false),
                        PhoneNumber = c.String(maxLength: 8000, unicode: false),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256, unicode: false),
                        CompanyName = c.String(maxLength: 8000, unicode: false),
                        CNPJ = c.String(maxLength: 8000, unicode: false),
                        Legal = c.String(maxLength: 8000, unicode: false),
                        ShareCapital = c.String(maxLength: 8000, unicode: false),
                        CurrentBalance = c.Decimal(precision: 13, scale: 2),
                        UniqueLeadBalance = c.Int(),
                        ProviderPlan = c.Int(),
                        EmployersNumber = c.Int(),
                        FoundationDate = c.DateTime(),
                        Certification = c.String(maxLength: 8000, unicode: false),
                        ResumeSent = c.Boolean(),
                        BirthDate = c.DateTime(),
                        Nationality = c.String(maxLength: 8000, unicode: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Address_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.Address_Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Address_Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128, unicode: false),
                        ClaimType = c.String(maxLength: 8000, unicode: false),
                        ClaimValue = c.String(maxLength: 8000, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128, unicode: false),
                        ProviderKey = c.String(nullable: false, maxLength: 128, unicode: false),
                        UserId = c.String(nullable: false, maxLength: 128, unicode: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128, unicode: false),
                        RoleId = c.String(nullable: false, maxLength: 128, unicode: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Specialty",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 8000, unicode: false),
                        Category = c.String(nullable: false, maxLength: 8000, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Course",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 8000, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GraduatedIn",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Course = c.String(nullable: false, maxLength: 8000, unicode: false),
                        Title = c.String(maxLength: 8000, unicode: false),
                        ConclusionYear = c.Int(nullable: false),
                        EducationalInstitution = c.String(maxLength: 8000, unicode: false),
                        Worker_Id = c.String(maxLength: 128, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Worker_Id)
                .Index(t => t.Worker_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, unicode: false),
                        Name = c.String(nullable: false, maxLength: 256, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Subscription",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProviderPlan = c.Int(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        NextRenovationDate = c.DateTime(),
                        IsActive = c.Boolean(nullable: false),
                        IsCanceled = c.Boolean(nullable: false),
                        IsRenewed = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        CancelDate = c.DateTime(),
                        Company_Id = c.String(maxLength: 128, unicode: false),
                        ParentSubscription_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Company_Id)
                .ForeignKey("dbo.Subscription", t => t.ParentSubscription_Id)
                .Index(t => t.Company_Id)
                .Index(t => t.ParentSubscription_Id);
            
            CreateTable(
                "dbo.SpecialtyCompany",
                c => new
                    {
                        Specialty_Id = c.Int(nullable: false),
                        Company_Id = c.String(nullable: false, maxLength: 128, unicode: false),
                    })
                .PrimaryKey(t => new { t.Specialty_Id, t.Company_Id })
                .ForeignKey("dbo.Specialty", t => t.Specialty_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.Company_Id, cascadeDelete: true)
                .Index(t => t.Specialty_Id)
                .Index(t => t.Company_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.Subscription", "ParentSubscription_Id", "dbo.Subscription");
            DropForeignKey("dbo.Subscription", "Company_Id", "dbo.Users");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.GraduatedIn", "Worker_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "Address_Id", "dbo.Address");
            DropForeignKey("dbo.SpecialtyCompany", "Company_Id", "dbo.Users");
            DropForeignKey("dbo.SpecialtyCompany", "Specialty_Id", "dbo.Specialty");
            DropForeignKey("dbo.Address", "Company_Id", "dbo.Users");
            DropIndex("dbo.SpecialtyCompany", new[] { "Company_Id" });
            DropIndex("dbo.SpecialtyCompany", new[] { "Specialty_Id" });
            DropIndex("dbo.Subscription", new[] { "ParentSubscription_Id" });
            DropIndex("dbo.Subscription", new[] { "Company_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.GraduatedIn", new[] { "Worker_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "Address_Id" });
            DropIndex("dbo.Users", "UserNameIndex");
            DropIndex("dbo.Address", new[] { "Company_Id" });
            DropTable("dbo.SpecialtyCompany");
            DropTable("dbo.Subscription");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.GraduatedIn");
            DropTable("dbo.Course");
            DropTable("dbo.Specialty");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.Users");
            DropTable("dbo.Address");
        }
    }
}
