namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AjustesparaCadastro : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SpecialtyCompany", "Specialty_Id", "dbo.Specialty");
            DropForeignKey("dbo.SpecialtyCompany", "Company_Id", "dbo.Users");
            DropIndex("dbo.SpecialtyCompany", new[] { "Specialty_Id" });
            DropIndex("dbo.SpecialtyCompany", new[] { "Company_Id" });
            AddColumn("dbo.Users", "Instituição", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.Users", "Specialty_Id", c => c.Int());
            AddColumn("dbo.Specialty", "Worker_Id", c => c.String(maxLength: 128, unicode: false));
            AddColumn("dbo.GraduatedIn", "Company_Id", c => c.String(maxLength: 128, unicode: false));
            CreateIndex("dbo.Users", "Specialty_Id");
            CreateIndex("dbo.GraduatedIn", "Company_Id");
            CreateIndex("dbo.Specialty", "Worker_Id");
            AddForeignKey("dbo.Users", "Specialty_Id", "dbo.Specialty", "Id");
            AddForeignKey("dbo.Specialty", "Worker_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.GraduatedIn", "Company_Id", "dbo.Users", "Id");
            DropColumn("dbo.Users", "CompanyName");
            DropColumn("dbo.Users", "CNPJ");
            DropColumn("dbo.Users", "Legal");
            DropColumn("dbo.Users", "ShareCapital");
            DropColumn("dbo.Users", "CurrentBalance");
            DropColumn("dbo.Users", "UniqueLeadBalance");
            DropColumn("dbo.Users", "ProviderPlan");
            DropColumn("dbo.Users", "EmployersNumber");
            DropColumn("dbo.Users", "FoundationDate");
            DropColumn("dbo.Users", "Certification");
            DropColumn("dbo.Users", "ResumeSent");
            DropColumn("dbo.Users", "BirthDate");
            DropColumn("dbo.Users", "Nationality");
            DropTable("dbo.SpecialtyCompany");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SpecialtyCompany",
                c => new
                    {
                        Specialty_Id = c.Int(nullable: false),
                        Company_Id = c.String(nullable: false, maxLength: 128, unicode: false),
                    })
                .PrimaryKey(t => new { t.Specialty_Id, t.Company_Id });
            
            AddColumn("dbo.Users", "Nationality", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.Users", "BirthDate", c => c.DateTime());
            AddColumn("dbo.Users", "ResumeSent", c => c.Boolean());
            AddColumn("dbo.Users", "Certification", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.Users", "FoundationDate", c => c.DateTime());
            AddColumn("dbo.Users", "EmployersNumber", c => c.Int());
            AddColumn("dbo.Users", "ProviderPlan", c => c.Int());
            AddColumn("dbo.Users", "UniqueLeadBalance", c => c.Int());
            AddColumn("dbo.Users", "CurrentBalance", c => c.Decimal(precision: 13, scale: 2));
            AddColumn("dbo.Users", "ShareCapital", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.Users", "Legal", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.Users", "CNPJ", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.Users", "CompanyName", c => c.String(maxLength: 8000, unicode: false));
            DropForeignKey("dbo.GraduatedIn", "Company_Id", "dbo.Users");
            DropForeignKey("dbo.Specialty", "Worker_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "Specialty_Id", "dbo.Specialty");
            DropIndex("dbo.Specialty", new[] { "Worker_Id" });
            DropIndex("dbo.GraduatedIn", new[] { "Company_Id" });
            DropIndex("dbo.Users", new[] { "Specialty_Id" });
            DropColumn("dbo.GraduatedIn", "Company_Id");
            DropColumn("dbo.Specialty", "Worker_Id");
            DropColumn("dbo.Users", "Specialty_Id");
            DropColumn("dbo.Users", "Instituição");
            CreateIndex("dbo.SpecialtyCompany", "Company_Id");
            CreateIndex("dbo.SpecialtyCompany", "Specialty_Id");
            AddForeignKey("dbo.SpecialtyCompany", "Company_Id", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SpecialtyCompany", "Specialty_Id", "dbo.Specialty", "Id", cascadeDelete: true);
        }
    }
}
