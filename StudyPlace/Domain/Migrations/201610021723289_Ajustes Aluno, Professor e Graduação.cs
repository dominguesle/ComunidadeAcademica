namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AjustesAlunoProfessoreGraduação : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GraduatedIn", "Worker_Id", "dbo.Users");
            DropIndex("dbo.GraduatedIn", new[] { "Worker_Id" });
            DropColumn("dbo.GraduatedIn", "Worker_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GraduatedIn", "Worker_Id", c => c.String(maxLength: 128, unicode: false));
            CreateIndex("dbo.GraduatedIn", "Worker_Id");
            AddForeignKey("dbo.GraduatedIn", "Worker_Id", "dbo.Users", "Id");
        }
    }
}
