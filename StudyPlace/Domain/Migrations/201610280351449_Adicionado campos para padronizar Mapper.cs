namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdicionadocamposparapadronizarMapper : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Curso", c => c.String(maxLength: 8000, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Curso");
        }
    }
}
