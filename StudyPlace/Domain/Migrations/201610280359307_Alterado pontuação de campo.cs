namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alteradopontuaçãodecampo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Instituicao", c => c.String(maxLength: 8000, unicode: false));
            DropColumn("dbo.Users", "Instituição");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Instituição", c => c.String(maxLength: 8000, unicode: false));
            DropColumn("dbo.Users", "Instituicao");
        }
    }
}
