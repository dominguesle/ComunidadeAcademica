namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdicionadocamposdoCadastrodoPofessor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "PrincipalCourse", c => c.String(maxLength: 8000, unicode: false));
            AddColumn("dbo.Users", "Instituicao1", c => c.String(maxLength: 8000, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Instituicao1");
            DropColumn("dbo.Users", "PrincipalCourse");
        }
    }
}
