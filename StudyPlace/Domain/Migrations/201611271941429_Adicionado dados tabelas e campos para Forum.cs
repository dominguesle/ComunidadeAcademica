namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdicionadodadostabelasecamposparaForum : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Subscription", "Company_Id", "dbo.Users");
            DropForeignKey("dbo.Subscription", "ParentSubscription_Id", "dbo.Subscription");
            DropIndex("dbo.Subscription", new[] { "Company_Id" });
            DropIndex("dbo.Subscription", new[] { "ParentSubscription_Id" });
            CreateTable(
                "dbo.Forum",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NomeForum = c.String(maxLength: 8000, unicode: false),
                        Curso = c.String(maxLength: 8000, unicode: false),
                        CreateDate = c.DateTime(nullable: false),
                        AlunoCriador_Id = c.String(maxLength: 128, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AlunoCriador_Id)
                .Index(t => t.AlunoCriador_Id);
            
            DropTable("dbo.Subscription");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Subscription",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProviderPlan = c.Int(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        NextRenovationDate = c.DateTime(),
                        IsActive = c.Boolean(nullable: false),
                        IsCanceled = c.Boolean(nullable: false),
                        IsRenewed = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        CancelDate = c.DateTime(),
                        Company_Id = c.String(maxLength: 128, unicode: false),
                        ParentSubscription_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Forum", "AlunoCriador_Id", "dbo.Users");
            DropIndex("dbo.Forum", new[] { "AlunoCriador_Id" });
            DropTable("dbo.Forum");
            CreateIndex("dbo.Subscription", "ParentSubscription_Id");
            CreateIndex("dbo.Subscription", "Company_Id");
            AddForeignKey("dbo.Subscription", "ParentSubscription_Id", "dbo.Subscription", "Id");
            AddForeignKey("dbo.Subscription", "Company_Id", "dbo.Users", "Id");
        }
    }
}
