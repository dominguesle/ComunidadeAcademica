namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AjustetabelaMensagens : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Mensagens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Mensagem = c.String(maxLength: 8000, unicode: false),
                        Anexo = c.String(maxLength: 8000, unicode: false),
                        CreateDate = c.DateTime(nullable: false),
                        AlunoCriador_Id = c.String(maxLength: 128, unicode: false),
                        Forum_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AlunoCriador_Id)
                .ForeignKey("dbo.Forum", t => t.Forum_Id)
                .Index(t => t.AlunoCriador_Id)
                .Index(t => t.Forum_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Mensagens", "Forum_Id", "dbo.Forum");
            DropForeignKey("dbo.Mensagens", "AlunoCriador_Id", "dbo.Users");
            DropIndex("dbo.Mensagens", new[] { "Forum_Id" });
            DropIndex("dbo.Mensagens", new[] { "AlunoCriador_Id" });
            DropTable("dbo.Mensagens");
        }
    }
}
