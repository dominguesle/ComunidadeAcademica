namespace Domain.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    internal sealed class Configuration : DbMigrationsConfiguration<ScaffoldContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(ScaffoldContext context)
        {
            CoursesOfGraduations(context);
            DataOfSpecialtiesCompany(context);

            var passwordHash = new PasswordHasher();
            var password = passwordHash.HashPassword("admin@2016");
            
            context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "aluno" });
            context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "professor" });

            context.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Admin" });
            context.Users.AddOrUpdate(u => u.UserName, new UserBase
            {
                FullName = "Administrador",
                Email = "admin@studyplace.com",
                UserName = "admin@studyplace.com",
                PasswordHash = password,
                CreatedDateTime = DateTime.UtcNow,
                SecurityStamp = Guid.NewGuid().ToString(),

            });

            context.SaveChanges();

            var role = context.Roles.Include(r => r.Users).Where(u => u.Name == "Admin").FirstOrDefault();
            var admin = context.Users.Where(u => u.UserName == "admin@studyplace.com").FirstOrDefault();

            if (admin.Claims.FirstOrDefault(c => c.ClaimValue == "admin") == null)
                admin.Claims.Add(new IdentityUserClaim() { ClaimType = "Role", ClaimValue = "admin", UserId = admin.Id });


            if (!role.Users.Any(u => u.UserId == admin.Id))
                role.Users.Add(new IdentityUserRole { UserId = admin.Id, RoleId = role.Id });
        }

        #region Dados Iniciais para Tabela Cursos

        private void CoursesOfGraduations(ScaffoldContext context)
        {
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 1, Description = "Administra��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 2, Description = "Administra��o p�blica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 3, Description = "Agroecologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 4, Description = "Agronomia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 5, Description = "An�lise de Sistemas" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 6, Description = "Antropologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 7, Description = "Aquicultura" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 8, Description = "Arqueologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 9, Description = "Arquitetura e Urbanismo" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 10, Description = "Astronomia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 11, Description = "Biocombust�veis" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 12, Description = "Bioengenharia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 13, Description = "Biomedicina" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 14, Description = "Biotecnologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 15, Description = "Ci�ncia da Computa��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 16, Description = "Ci�ncias agr�rias" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 17, Description = "Ci�ncias ambientais" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 18, Description = "Ci�ncias biol�gicas (biologia)" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 19, Description = "Ci�ncias cont�beis" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 20, Description = "Ci�ncias econ�micas" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 21, Description = "Ci�ncias naturais" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 22, Description = "Ci�ncias pol�ticas" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 23, Description = "Ci�ncias sociais" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 24, Description = "Com�rcio exterior" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 25, Description = "Comunica��o social" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 26, Description = "Design" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 27, Description = "Direito" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 28, Description = "Ecologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 29, Description = "Enfermagem" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 30, Description = "Engenharia aeroespacial" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 31, Description = "Engenharia aeron�utica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 32, Description = "Engenharia agr�cola" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 33, Description = "Engenharia ambiental" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 34, Description = "Engenharia biof�sica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 35, Description = "Engenharia biom�dica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 36, Description = "Engenharia cartogr�fica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 37, Description = "Engenharia da qualidade" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 38, Description = "Engenharia de agrimensura" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 39, Description = "Engenharia de alimentos" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 40, Description = "Engenharia de computa��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 41, Description = "Engenharia de controle e automa��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 42, Description = "Engenharia de informa��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 43, Description = "Engenharia de instrumenta��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 44, Description = "Engenharia de manufatura" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 45, Description = "Engenharia de manuten��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 46, Description = "Engenharia de materiais" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 47, Description = "Engenharia de minas" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 48, Description = "Engenharia de pesca" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 49, Description = "Engenharia de petr�leo" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 50, Description = "Engenharia de produ��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 51, Description = "Engenharia de sistemas" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 52, Description = "Engenharia de software" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 53, Description = "Engenharia de telecomunica��es" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 54, Description = "Engenharia de transportes e log�stica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 55, Description = "Engenharia econ�mica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 56, Description = "Engenharia el�trica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 57, Description = "Engenharia eletr�nica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 58, Description = "Engenharia estrutural" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 59, Description = "Engenharia f�sica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 60, Description = "Engenharia florestal" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 61, Description = "Engenharia geogr�fica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 62, Description = "Engenharia geol�gica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 63, Description = "Engenharia hidr�ulica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 64, Description = "Engenharia humana" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 65, Description = "Engenharia industrial" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 66, Description = "Engenharia industrial madeireira" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 67, Description = "Engenharia mec�nica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 68, Description = "Engenharia mecatr�nica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 69, Description = "Engenharia metal�rgica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 70, Description = "Engenharia militar" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 71, Description = "Engenharia naval e oce�nica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 72, Description = "Engenharia nuclear" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 73, Description = "Engenharia qu�mica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 74, Description = "Engenharia sanit�ria" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 75, Description = "Engenharia t�xtil" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 76, Description = "Engenharia urbana" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 77, Description = "Estat�stica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 78, Description = "Farm�cia e Bioqu�mica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 79, Description = "F�sica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 80, Description = "Fisioterapia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 81, Description = "Fonoaudiologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 82, Description = "Geoci�ncias" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 83, Description = "Geof�sica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 84, Description = "Geografia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 85, Description = "Geologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 86, Description = "Gest�o de pol�ticas p�blicas" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 87, Description = "Inform�tica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 88, Description = "Jornalismo" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 89, Description = "Marketing" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 90, Description = "Matem�tica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 91, Description = "Medicina" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 92, Description = "Medicina veterin�ria" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 93, Description = "Meteorologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 94, Description = "Nutri��o" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 95, Description = "Oceanografia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 96, Description = "Odontologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 97, Description = "Paisagismo" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 98, Description = "Pedagogia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 99, Description = "Processamento de dados" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 100, Description = "Psicologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 101, Description = "Publicidade" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 102, Description = "Qu�mica" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 103, Description = "Qu�mica ambiental" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 104, Description = "Qu�mica industrial" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 105, Description = "Rela��es p�blicas" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 106, Description = "Servi�o social" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 107, Description = "Turismologia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 108, Description = "Zootecnia" });
            context.Courses.AddOrUpdate(g => g.Id, new Course { Id = 109, Description = "Outro" });
        }


        #endregion

        #region Dados Iniciais para Tabela de Especilidades de Empresas

        private void DataOfSpecialtiesCompany(ScaffoldContext context)
        {
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 1, Category = "SERVI�OS DE ENGENHARIA", Description = "ASSESSORIA T�CNICA EM CONSTRU��O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 2, Category = "SERVI�OS DE ENGENHARIA", Description = "ASSIST�NCIA T�CNICA NA �REA DE ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 3, Category = "SERVI�OS DE ENGENHARIA", Description = "AVALIA��O, PER�CIA E INSPE��O EM ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 4, Category = "SERVI�OS DE ENGENHARIA", Description = "BIOENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 5, Category = "SERVI�OS DE ENGENHARIA", Description = "CALCULISTA EM CONSTRU��O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 6, Category = "SERVI�OS DE ENGENHARIA", Description = "CONSULTORIA EM ENGENHARIA CIVIL, NAVAL, EL�TRICA, ELETR�NICA, HIDR�ULICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 7, Category = "SERVI�OS DE ENGENHARIA", Description = "CONSULTORIA EM ENGENHARIA DE OBRAS EM ESTRADAS, OBRAS HIDR�ULICAS E URBANAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 8, Category = "SERVI�OS DE ENGENHARIA", Description = "CONSULTORIA EM ENGENHARIA DE TR�FEGO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 9, Category = "SERVI�OS DE ENGENHARIA", Description = "C�LCULO ESTRUTURAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 10, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA AMBIENTAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 11, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA AUTOMOTIVA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 12, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA CONSULTIVA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 13, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA DE PROJETOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 14, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA DE TRANSPORTE" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 15, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA DE TR�NSITO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 16, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA NAVAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 17, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA PORTU�RIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 18, Category = "SERVI�OS DE ENGENHARIA", Description = "ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 19, Category = "SERVI�OS DE ENGENHARIA", Description = "ESCRIT�RIO DE PROJETISTA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 20, Category = "SERVI�OS DE ENGENHARIA", Description = "ESCRIT�RIO DE PROJETOS DE ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 21, Category = "SERVI�OS DE ENGENHARIA", Description = "FISCALIZA��O DE OBRAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 22, Category = "SERVI�OS DE ENGENHARIA", Description = "GERENCIAMENTO DA ELABORA��O DE PROJETOS DE ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 23, Category = "SERVI�OS DE ENGENHARIA", Description = "INSPE��O T�CNICA DE ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 24, Category = "SERVI�OS DE ENGENHARIA", Description = "M�QUINARIA E INSTALA��ES INDUSTRIAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 25, Category = "SERVI�OS DE ENGENHARIA", Description = "PLANEJAMENTO DE OBRAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 26, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS AERON�UTICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 27, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE ACONDICIONAMENTO DE AR, REFRIGERA��O, SANEAMENTO, CONTROLE DE CONTAMINA��O E ENGENHARIA AC�STICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 28, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE EDIF�CIOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 29, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE ENGENHARIA AMBIENTAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 30, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE ENGENHARIA CIVIL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 31, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE ENGENHARIA DE TR�FEGO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 32, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE ENGENHARIA ELETR�NICA, DE MINAS, QU�MICA, MEC�NICA, INDUSTRIAL, DE SISTEMAS E DE SEGURAN�A, AGR�RIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 33, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 34, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE INSTALA��ES ESPORTIVAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 35, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE OBRAS VI�RIAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 36, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS DE RODOFERROVIAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 37, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS ESTRUTURAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 38, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS HIDR�ULICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 39, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS NA CONSTRU��O CIVIL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 40, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS PARA CAPTA��O E DISTRIBUI��O DE �GUA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 41, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS PARA INFRA-ESTRUTURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 42, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS PARA INSTALA��ES EL�TRICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 43, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS PARA REDES DE TELEFONIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 44, Category = "SERVI�OS DE ENGENHARIA", Description = "PROJETOS PARA TELECOMUNICA��ES" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 45, Category = "SERVI�OS DE ENGENHARIA", Description = "SUPERVIS�O DE OBRAS POR ENGENHEIROS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 46, Category = "SERVI�OS DE ENGENHARIA", Description = "SUPERVIS�O DO PROJETO DE CONSTRU��O" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 47, Category = "SERVI�OS DE ARQUITETURA", Description = "ARQUITETURA PAISAG�STICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 48, Category = "SERVI�OS DE ARQUITETURA", Description = "ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 49, Category = "SERVI�OS DE ARQUITETURA", Description = "ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 50, Category = "SERVI�OS DE ARQUITETURA", Description = "CONSULTORIA, ASSESSORIA EM ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 51, Category = "SERVI�OS DE ARQUITETURA", Description = "ESCRIT�RIO DE ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 52, Category = "SERVI�OS DE ARQUITETURA", Description = "ESCRIT�RIO DE PROJETISTA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 53, Category = "SERVI�OS DE ARQUITETURA", Description = "GERENCIAMENTO DE PROJETO DE ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 54, Category = "SERVI�OS DE ARQUITETURA", Description = "PAISAGISMO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 55, Category = "SERVI�OS DE ARQUITETURA", Description = "PROJETOS ARQUITET�NICOS DE JARDINS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 56, Category = "SERVI�OS DE ARQUITETURA", Description = "PROJETOS ARQUITET�NICOS E PAISAG�STICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 57, Category = "SERVI�OS DE ARQUITETURA", Description = "PROJETOS DE ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 58, Category = "SERVI�OS DE ARQUITETURA", Description = "PROJETOS DE ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 59, Category = "SERVI�OS DE ARQUITETURA", Description = "PROJETOS DE INSTALA��ES ESPORTIVAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 60, Category = "SERVI�OS DE ARQUITETURA", Description = "PROJETOS PARA ORDENA��O URBANA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 61, Category = "SERVI�OS DE ARQUITETURA", Description = "URBANISMO" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 62, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "AGRIMENSURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 63, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "BATIMETRIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 64, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "CARTOGRAFIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 65, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "CARTOGR�FICOS, SERVI�OS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 66, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "ESTUDOS E DEMARCA��O DE SOLOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 67, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "ESTUDOS GEOD�SICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 68, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "ESTUDOS TOPOGR�FICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 69, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "GEOD�SIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 70, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "GEOPROCESSAMENTO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 71, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "HIDROMETRIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 72, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "INFORMA��O CARTOGR�FICA E ESPACIAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 73, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "LEVANTAMENTOS BATIM�TRICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 74, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "LEVANTAMENTOS GEOD�SICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 75, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "LEVANTAMENTOS S�SMICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 76, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "LIMITES TOPOGR�FICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 77, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "PROJETOS DE TOPOGRAFIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 78, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "PROJETOS TOPOGR�FICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 79, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "TOPOGRAFIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 80, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "AEROFOTOGRAMETRIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 81, Category = "SERVI�OS DE CARTOGRAFIA, TOPOGRAFIA E GEOD�SIA", Description = "AEROLEVANTAMENTOS" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 82, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "ANALISES DE SOLO PARA INVESTIGA��O GEOL�GICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 83, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "ESTUDOS GEOT�CNICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 84, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "ESTUDOS SISMOGR�FICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 85, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "LEVANTAMENTOS GEOL�GICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 86, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "LEVANTAMENTOS, ESTUDOS GEOF�SICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 87, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "LEVANTAMENTOS, ESTUDOS HIDROGR�FICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 88, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "PERFILAGEM DE SOLO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 89, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "PROSPEC��O DE PETR�LEO E G�S NATURAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 90, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "PROSPEC��O GEOL�GICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 91, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "PROSPEC��O S�SMICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 92, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "PROSPEC��O, PESQUISA MINERAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 93, Category = "SERVI�OS DE ESTUDOS GEOL�GICOS", Description = "SERVI�OS T�CNICOS EM GEOLOGIA" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 94, Category = "SERVI�OS DE DESENHO T�CNICO RELACIONADOS � ARQUITETURA E ENGENHARIA", Description = "COMPUTA��O GR�FICA PARA A CRIA��O DE MAQUETES" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 95, Category = "SERVI�OS DE DESENHO T�CNICO RELACIONADOS � ARQUITETURA E ENGENHARIA", Description = "COMPUTA��O GR�FICA PARA A CRIA��O DE PLANTAS HUMANIZADAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 96, Category = "SERVI�OS DE DESENHO T�CNICO RELACIONADOS � ARQUITETURA E ENGENHARIA", Description = "CONFEC��O DE MAQUETES PARA ENGENHARIA E ARQUITETURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 97, Category = "SERVI�OS DE DESENHO T�CNICO RELACIONADOS � ARQUITETURA E ENGENHARIA", Description = "DESENHO T�CNICO ESPECIALIZADO PARA ARQUITETURA E ENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 98, Category = "SERVI�OS DE DESENHO T�CNICO RELACIONADOS � ARQUITETURA E ENGENHARIA", Description = "DESENHOS DE ARQUITETURA E ENGENHARIA" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 99, Category = "SERVI�OS DE PER�CIA T�CNICA RELACIONADOS � SEGURAN�A DO TRABALHO", Description = "PER�CIA T�CNICA RELACIONADA � SEGURAN�A DO TRABALHO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 100, Category = "SERVI�OS DE PER�CIA T�CNICA RELACIONADOS � SEGURAN�A DO TRABALHO", Description = "PROJETOS DE SEGURAN�A DO TRABALHO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 101, Category = "SERVI�OS DE PER�CIA T�CNICA RELACIONADOS � SEGURAN�A DO TRABALHO", Description = "SEGURAN�A DO TRABALHO AFERI��O DE BALAN�AS" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 102, Category = "TESTES E ANALISES T�CNICAS", Description = "AFERI��O DE INSTRUMENTOS DE MEDIDAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 103, Category = "TESTES E ANALISES T�CNICAS", Description = "AFERI��O METROL�GICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 104, Category = "TESTES E ANALISES T�CNICAS", Description = "ALIMENTOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 105, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISE BACTERIOL�GICA DA �GUA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 106, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISE DE QUALIDADE" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 107, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISE DE RESIST�NCIA DE INSTALA��ES E MATERIAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 108, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISE E AMOSTRAGEM DE MIN�RIO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 109, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES BIOTECNOL�GICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 110, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES BROMATOL�GICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 111, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES CROMATOGR�FICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 112, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES DE CONCRETO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 113, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES DE PROCESSOS MINERAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 114, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES DE SEMENTES" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 115, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES DE VIBRA��O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 116, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES DE �LEOS MINERAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 117, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISE DE SOLOS, SERVI�OS DE" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 118, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES F�SICO-QU�MICA DA �GUA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 119, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES MICROBIOL�GICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 120, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES QUIMICO-BIOL�GICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 121, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES QU�MICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 122, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES TECNOL�GICAS DE CONCRETO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 123, Category = "TESTES E ANALISES T�CNICAS", Description = "AN�LISES, TESTES BIOL�GICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 124, Category = "TESTES E ANALISES T�CNICAS", Description = "CALIBRA��ES DE INSTRUMENTOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 125, Category = "TESTES E ANALISES T�CNICAS", Description = "CERTIFICADOS DE HOMOLOGA��O DE BARCOS, AVI�ES, VE�CULOS MOTORIZADOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 126, Category = "TESTES E ANALISES T�CNICAS", Description = "CERTIFICADOS DE HOMOLOGA��O DE PROJETOS NUCLEARES" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 127, Category = "TESTES E ANALISES T�CNICAS", Description = "CERTIFICADOS DE HOMOLOGA��O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 128, Category = "TESTES E ANALISES T�CNICAS", Description = "CERTIFICADOS DE VISTORIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 129, Category = "TESTES E ANALISES T�CNICAS", Description = "CERTIFICA��O DE SEGURAN�A VEICULAR" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 130, Category = "TESTES E ANALISES T�CNICAS", Description = "CONTROLE DE NORMAS T�CNICAS LEGAIS NAS CONSTRU��ES" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 131, Category = "TESTES E ANALISES T�CNICAS", Description = "CONTROLE DE QUALIDADE DE PRODUTOS ALIMENT�CIOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 132, Category = "TESTES E ANALISES T�CNICAS", Description = "CONTROLE TECNOL�GICO DE MATERIAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 133, Category = "TESTES E ANALISES T�CNICAS", Description = "DETEC��O DE VAZAMENTOS EM TUBULA��ES DE G�S, �GUA, COMBUST�VEIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 134, Category = "TESTES E ANALISES T�CNICAS", Description = "ENSAIO E INSPE��ES DE MATERIAIS E DE PRODUTOS PARA FINS DE CUMPRIMENTO DE NORMAS LEGAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 135, Category = "TESTES E ANALISES T�CNICAS", Description = "ENSAIOS E AN�LISES F�SICAS PARA FINS DE CUMPRIMENTO DE NORMAS LEGAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 136, Category = "TESTES E ANALISES T�CNICAS", Description = "ENSAIOS E AN�LISES QU�MICOS E BIOL�GICOS PARA FINS DE CUMPRIMENTO DE NORMAS LEGAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 137, Category = "TESTES E ANALISES T�CNICAS", Description = "ENSAIOS METROL�GICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 138, Category = "TESTES E ANALISES T�CNICAS", Description = "ENSAIOS TECNOL�GICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 139, Category = "TESTES E ANALISES T�CNICAS", Description = "EXAMES, ENSAIOS BROMATOL�GICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 140, Category = "TESTES E ANALISES T�CNICAS", Description = "GAMAGRAFIAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 141, Category = "TESTES E ANALISES T�CNICAS", Description = "G�S NATURAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 142, Category = "TESTES E ANALISES T�CNICAS", Description = "INSPE��O DE EQUIPAMENTO DE G�S NATURAL PARA SEGURAN�A VEICULAR" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 143, Category = "TESTES E ANALISES T�CNICAS", Description = "LABORAT�RIO DE AN�LISE DE ALIMENTOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 144, Category = "TESTES E ANALISES T�CNICAS", Description = "LABORAT�RIO DE AN�LISE MINERAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 145, Category = "TESTES E ANALISES T�CNICAS", Description = "LABORAT�RIO DE ENSAIOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 146, Category = "TESTES E ANALISES T�CNICAS", Description = "LABORAT�RIOS POLICIAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 147, Category = "TESTES E ANALISES T�CNICAS", Description = "MEDI��O DA POLUI��O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 148, Category = "TESTES E ANALISES T�CNICAS", Description = "MEDI��O DA PUREZA DA �GUA OU AR" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 149, Category = "TESTES E ANALISES T�CNICAS", Description = "MEDI��O DE RADIOATIVIDADE" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 150, Category = "TESTES E ANALISES T�CNICAS", Description = "METROLOGIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 151, Category = "TESTES E ANALISES T�CNICAS", Description = "MONITORAMENTO AMBIENTAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 152, Category = "TESTES E ANALISES T�CNICAS", Description = "RADIOGRAFIA E ULTRA-SOM INDUSTRIAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 153, Category = "TESTES E ANALISES T�CNICAS", Description = "TESTES AUTOMOTIVOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 154, Category = "TESTES E ANALISES T�CNICAS", Description = "TESTES E ENSAIOS DE SOLOS PARA CARACTERIZA��O DE SUAS PROPRIEDADES F�SICO-QU�MICAS, TAIS COMO: CONDUTIVIDADE EL�TRICA, MEDI��O DE TEORES DE SAIS E MINERAIS, GRANULOMETRIA, DENSIDADE, ETC." });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 155, Category = "TESTES E ANALISES T�CNICAS", Description = "TESTES EM MATERIAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 156, Category = "TESTES E ANALISES T�CNICAS", Description = "TESTES F�SICOS, QU�MICOS DE MATERIAIS E PRODUTOS" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 157, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA CREDIT�CIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 158, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA DE IMPRENSA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 159, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA E CONSULTORIA EM RECURSOS HUMANOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 160, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA EM COMUNICA��O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 161, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA EM GEST�O EMPRESARIAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 162, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA EMPRESARIAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 163, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA � GEST�O HOSPITALAR" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 164, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA �S EMPRESAS EM QUEST�ES DE GEST�O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 165, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA �S EMPRESAS EM QUEST�ES FINANCEIRAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 166, Category = "GEST�O EMPRESARIAL", Description = "ASSESSORIA, ORIENTA��O E ASSIST�NCIA PRESTADA �S EMPRESAS EM MAT�RIA DE PLANEJAMENTO, ORGANIZA��O, REENGENHARIA, CONTROLE E GEST�O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 167, Category = "GEST�O EMPRESARIAL", Description = "CONSULTORIA A EMPRESAS EM COM�RCIO EXTERIOR" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 168, Category = "GEST�O EMPRESARIAL", Description = "CONSULTORIA EM GEST�O DE EMPRESAS AGROPECU�RIAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 169, Category = "GEST�O EMPRESARIAL", Description = "CONSULTORIA EM NEGOCIA��O TRABALHISTA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 170, Category = "GEST�O EMPRESARIAL", Description = "CONSULTORIA EM RELA��ES P�BLICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 171, Category = "GEST�O EMPRESARIAL", Description = "CONSULTORIA FINANCEIRA A EMPRESAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 172, Category = "GEST�O EMPRESARIAL", Description = "CONSULTORIA NA ADMINISTRA��O DE EMPRESAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 173, Category = "GEST�O EMPRESARIAL", Description = "CONSULTORIA NA �REA ECON�MICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 174, Category = "GEST�O EMPRESARIAL", Description = "CONTROLE OR�AMENT�RIO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 175, Category = "GEST�O EMPRESARIAL", Description = "GEST�O EMPRESARIAL" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 176, Category = "GEST�O EMPRESARIAL", Description = "INTERMEDI�RIO DE CONTRATA��O DE OBRAS P�BLICAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 177, Category = "GEST�O EMPRESARIAL", Description = "LOBISTA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 178, Category = "GEST�O EMPRESARIAL", Description = "LOG�STICA DE LOCALIZA��O" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 179, Category = "GEST�O EMPRESARIAL", Description = "REENGENHARIA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 180, Category = "GEST�O EMPRESARIAL", Description = "RELA��ES P�BLICAS" });

            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 181, Category = "OUTROS", Description = "ASSESSORIA E CONSULTORIA EM SA�DE E MEDICINA DO TRABALHO" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 182, Category = "OUTROS", Description = "ASSESSORIA EM ATIVIDADES AGR�COLAS E PECU�RIAS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 183, Category = "OUTROS", Description = "ASSESSORIA, CONSULTORIA, ORIENTA��O E ASSIST�NCIA NA AGRICULTURA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 184, Category = "OUTROS", Description = "ASSESSORIA E CONSULTORIA EM PROJETOS SOCIAIS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 185, Category = "OUTROS", Description = "ASSESSORIA E CONSULTORIA EM PROJETOS ECON�MICOS" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 186, Category = "OUTROS", Description = "ASSESSORIA E CONSULTORIA EM QUEST�ES DE SUSTENTABILIDADE DO MEIO AMBIENTE" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 187, Category = "OUTROS", Description = "ASSESSORIA E CONSULTORIA NA �REA DE ESTAT�STICA" });
            context.Specialties.AddOrUpdate(a => a.Id, new Specialty { Id = 188, Category = "OUTROS", Description = "METEOROLOGIA" });


        }

        #endregion
        
    }
}