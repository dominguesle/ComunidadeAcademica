﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Address
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Street { get; set; }
        
        public string Number { get; set; }

        public string Complement { get; set; }

        public string CEP { get; set; }

        public string Neighborhood { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        public bool FilialAddress { get; set; }
        
        public virtual Aluno Company { get; set; }        
    }
}
