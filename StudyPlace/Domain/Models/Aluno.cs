﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace Domain.Models
{
    public class Aluno : UserBase
    {
        public Aluno()
        {
            this.CreatedDateTime = DateTime.UtcNow;
        }
        
        public virtual string Instituicao { get; set; }

        //public virtual List<Address> Addresses { get; set; }

        public virtual string Curso { get; set; }

        public void AddClaimsToUser()
        {
            this.Claims.Add(new IdentityUserClaim
            {
                ClaimType = "Role",
                ClaimValue = "aluno",
                UserId = this.Id
            });
        }
    }
}
