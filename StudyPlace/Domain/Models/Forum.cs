﻿using Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Forum
    {
        public Forum()
        {
            this.CreateDate = DateTime.UtcNow;
        }

        [Key]
        public int Id { get; set; }

        public string NomeForum { get; set; }

        public string Curso { get; set; }

        public virtual Aluno AlunoCriador { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
