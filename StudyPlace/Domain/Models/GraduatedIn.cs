﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class GraduatedIn
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        public string Course { get; set; }

        public string Title { get; set; }

        public int ConclusionYear { get; set; }

        public string EducationalInstitution { get; set; }
        
        public virtual Aluno Aluno { get; set; }

        //public virtual List<Company> Companies { get; set; }
    }
}
