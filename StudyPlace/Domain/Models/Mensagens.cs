﻿using Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Mensagens
    {
        public Mensagens()
        {
            this.CreateDate = DateTime.UtcNow;
        }

        [Key]
        public int Id { get; set; }

        public string Mensagem { get; set; }

        public string Anexo { get; set; }

        public virtual Forum Forum { get; set; }

        public virtual Aluno AlunoCriador { get; set; }

        public DateTime CreateDate { get; set; }
    }
}