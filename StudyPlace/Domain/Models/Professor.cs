﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace Domain.Models
{

    public class Professor : UserBase
    {
        public Professor()
        {
            this.CreatedDateTime = DateTime.UtcNow;
        }

        public string PrincipalCourse { get; set; }

        public string Instituicao { get; set; }

       public virtual List<Specialty> Specialties { get; set; }

        public void AddClaimsToUser()
        {
            this.Claims.Add(new IdentityUserClaim
            {
                ClaimType = "Role",
                ClaimValue = "professor",
                UserId = this.Id
            });
        }
    }
}
