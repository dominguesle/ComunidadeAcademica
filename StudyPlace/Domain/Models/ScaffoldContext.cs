﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;

namespace Domain.Models
{
    public class ScaffoldContext : IdentityDbContext<UserBase>
    {
        public ScaffoldContext() : base("StudyPlace", throwIfV1Schema: false) { }

        public static ScaffoldContext Create()
        {
            return new ScaffoldContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties().Where(x => x.Name == "Id").Configure(x => x.IsKey());

            modelBuilder.Properties<decimal>().Configure(x => x.HasPrecision(13, 2));
            modelBuilder.Properties<string>().Configure(x => x.HasColumnType("varchar"));            

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserBase>().ToTable("Users");
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        //public IDbSet<UserBase> Users { get; set; }
        public IDbSet<Professor> Workers { get; set; }
        public IDbSet<Aluno> Companies { get; set; }
        public IDbSet<Address> Addresses { get; set; }        
        public IDbSet<Course> Courses { get; set; }
        public IDbSet<Specialty> Specialties { get; set; }        
        public IDbSet<GraduatedIn> Graduations { get; set; }
        public IDbSet<Forum> Foruns { get; set; }
        public IDbSet<Mensagens> Mensagens { get; set; }
    }
}
