﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Specialty
    {
        [Key]
        public int Id { get; set; }

        [Required]        
        public string Description { get; set; }

        [Required]
        public string Category { get; set; }

        public virtual List<Aluno> Companies { get; set; }        
    }
}
