﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class UserBase : IdentityUser
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }
        
        public string ImageUrl { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UserBase> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        public void HashPassword(string password)
        {
            PasswordHash = new PasswordHasher().HashPassword(password);
            SecurityStamp = Guid.NewGuid().ToString();
        }

        public void SetUserName()
        {
            this.UserName = this.Email;
        }
    }
}