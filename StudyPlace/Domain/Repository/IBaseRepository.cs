﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.Repository
{
    public interface IBaseRepository<T> : IDisposable where T : class
    {
        T GetById(object id);
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
        IQueryable<T> List { get; }
        void Attach(object entity);
        void SetObjectState(object entity, EntityState state);        
        IQueryable<T> Where(Expression<Func<T, bool>> predicate);
    }
}
