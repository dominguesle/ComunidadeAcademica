﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IIdentityRepository<T> where T : class
    {
        IEnumerable<T> List { get; }
        T Get(string id);
        bool Persist(T model);
        bool Delete(string id);
    }
}
