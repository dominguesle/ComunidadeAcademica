﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Configuration;
using System.IO;

namespace Services.Helpers
{
    public class AzureFileHelper
    {
        public static string SaveFile(string fileName, Stream fileStream)
        {
            try
            {
                string connectionString = ConfigurationManager.AppSettings["AzureStorageConnectionString"];
                string container = ConfigurationManager.AppSettings["AzureBaseContainer"].ToLower();

                // Blob client.
                var storageAccount = CloudStorageAccount.Parse(connectionString);
                var blobClient = storageAccount.CreateCloudBlobClient();

                // Main container.
                var containerRef = blobClient.GetContainerReference(container);
                if (!containerRef.Exists())
                {
                    containerRef.CreateIfNotExists();
                    containerRef.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Blob });
                }

                // Upload blob.
                var blockBlob = containerRef.GetBlockBlobReference(fileName);

                if (fileStream.CanSeek)
                    fileStream.Position = 0;

                blockBlob.UploadFromStream(fileStream);

                return fileName;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
