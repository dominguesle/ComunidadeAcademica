﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Helpers
{
    public static class EnumHelper
    {
        public static string EnumToString(this Enum enumValue)
        {
            var enumType = enumValue.GetType();
            var fieldInfo = enumType.GetField(enumValue.ToString());
            var descriptionAttr = Attribute.GetCustomAttribute(fieldInfo, typeof(DescriptionAttribute)) as DescriptionAttribute;

            if (descriptionAttr == null)
                return enumValue.ToString();

            return descriptionAttr.Description;
        }
    }
}
