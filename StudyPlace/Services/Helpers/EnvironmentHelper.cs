﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Helpers
{
    public class EnvironmentHelper
    {
        private static EnvironmentHelper instance = new EnvironmentHelper();
        private string environment = "";

        private EnvironmentHelper() 
        {
            var value = ConfigurationManager.AppSettings["Environment"];

            if(value != null)
                environment = value.ToString();
        }

        public static EnvironmentHelper Instance()
        {
            return instance;
        }

        public string GetKey(string key)
        {
            var value = ConfigurationManager.AppSettings[key + environment];

            return value != null ? value.ToString() : null;
        }

        public string GetEnvironment()
        {
            return environment;
        }
    }
}
