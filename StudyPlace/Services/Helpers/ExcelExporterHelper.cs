﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Helpers
{
    public class ExcelExporterHelper
    {
        public static Stream Export(DataTable dataTable)
        {
            var book = CreateWorkbook(dataTable);
            var stream = new MemoryStream();

            book.SaveAs(stream);
            stream.Position = 0;

            return stream;
        }

        private static XLWorkbook CreateWorkbook(DataTable dataTable)
        {
            if (String.IsNullOrEmpty(dataTable.TableName))
                dataTable.TableName = "Dados";

            var book = new XLWorkbook();
            var sheet = book.Worksheets.Add(dataTable);

            return book;
        }
    }
}
