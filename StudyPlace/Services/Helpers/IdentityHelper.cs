﻿using Domain.Models;
using Services.Repository;
using System.Security.Principal;
using System.Linq;

namespace Services.Helpers
{
    public static class IdentityHelper
    {
        private static UserBase user;

        public static string GetFullName(this IIdentity identity)
        {
            if(user == null || (user.Email != identity.Name))
                user = new Repository<UserBase>(new ScaffoldContext()).List.FirstOrDefault(u => u.Email == identity.Name);
            
            return user != null ? user.FullName : "";
        }
    }
}
