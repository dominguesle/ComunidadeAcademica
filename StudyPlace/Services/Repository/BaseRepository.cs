﻿using Domain.Models;

namespace Services.Repository
{
    public class BaseRepository
    {
        protected ScaffoldContext Context { get; set; }

        public BaseRepository(ScaffoldContext context)
        {
            Context = context;
        }
    }
}
