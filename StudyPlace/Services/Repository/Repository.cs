﻿using Domain.Models;
using Domain.Repository;
using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace Services.Repository
{
    public class Repository<T> : BaseRepository, IBaseRepository<T> where T : class
    {
        private IDbSet<T> _entities;

        public Repository(ScaffoldContext context) : base(context)
        {
        }

        private IDbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = Context.Set<T>();
                }
                return _entities;
            }
        }

        public T GetById(object id)
        {
            var entity = this.Entities.Find(id);
            Context.Entry(entity).State = EntityState.Detached;

            return entity;
        }

        public void Insert(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                this.Entities.Add(entity);
                this.Context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += string.Format("Propriedade: {0} Erro: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }

                var fail = new Exception(msg, dbEx);
                throw fail;
            }
        }

        public void Update(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                this.Context.Entry(entity).State = EntityState.Modified;
                this.Context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += Environment.NewLine + string.Format("Propriedade: {0} Erro: {1}",
                        validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                var fail = new Exception(msg, dbEx);
                throw fail;
            }
        }

        public void Delete(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                this.Context.Entry(entity).State = EntityState.Deleted;
                this.Context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        msg += Environment.NewLine + string.Format("Propriedade: {0} Erro: {1}",
                        validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                var fail = new Exception(msg, dbEx);
                throw fail;
            }
        }

        public IQueryable<T> List
        {
            get { return this.Entities; }
        }

        public int Count()
        {
            return this.Entities.Count();
        }

        public void Attach(object entity)
        {
            this.Context.Entry(entity).State = EntityState.Unchanged;
        }

        public void SetObjectState(object entity, EntityState state)
        {
            this.Context.Entry(entity).State = state;
        }
        
        public IQueryable<T> Where(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = this.Entities.Where(predicate);
            return query;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
