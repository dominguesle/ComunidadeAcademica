﻿using Domain.Repository;
using Domain.Services;
using System;
using System.Collections.Generic;

namespace Services.Services
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> _repository;

        public BaseService(IBaseRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public void Add(TEntity entity)
        {
            _repository.Insert(entity);
        }

        public void Update(TEntity entity)
        {
            _repository.Update(entity);
        }
        public void Remove(TEntity entity)
        {
            _repository.Delete(entity);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.Get();
        }

        public TEntity GetById(object id)
        {
            return _repository.Get(id);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
