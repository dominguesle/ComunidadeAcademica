﻿using Domain.Interfaces.Repository;
using Domain.Interfaces.Services;
using Domain.Models;
using Services.Services;
using System.Collections.Generic;
using System.Linq;

namespace Domain.Services
{
    public class CompanyService : BaseService<Company>, ICompanyService
    {
        private readonly ICompanyRepository _repository;

        public CompanyService(ICompanyRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public List<Company> GetAllOrderByDataDescending()
        {
            return _repository.Get().OrderByDescending(c => c.CreatedDateTime).ToList();
        }

        public Company GetByUserId(string userId)
        {            
            return _repository.Get(userId);
        }
    }
}
