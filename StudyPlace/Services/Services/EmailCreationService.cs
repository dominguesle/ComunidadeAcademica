﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.Services
{
    public class EmailCreationService
    {
        public IdentityMessage message = new IdentityMessage();

        public EmailCreationService() { }

        public void LoadTemplate(string templateName)
        {
            message.Body = File.ReadAllText(HttpRuntime.AppDomainAppPath + "\\Emails\\" + templateName + ".html");
        }

        public string LoadParcialTemplate(string templateName)
        {
            return File.ReadAllText(HttpRuntime.AppDomainAppPath + "\\Emails\\" + templateName + ".html");
        }

        public string ReplaceVarsPartial(string baseContentPartial, string varName, string varValue)
        {
            return baseContentPartial.Replace("{{" + varName + "}}", varValue);
        }

        public void ReplaceVars(string varName, string varValue)
        {
            message.Body = message.Body.Replace("{{" + varName + "}}", varValue);
        }
        
    }
}
