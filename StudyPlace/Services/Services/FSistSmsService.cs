﻿using Domain.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class FSistSmsService : ISmsService
    {
        public Task DeliverAsync(IdentityMessage message)
        {
            var client = new WebClient();
            var fSistMessage = new FSistSmsMessage();

            fSistMessage.Message = message.Body;
            fSistMessage.PhoneNumber = message.Destination;

            return client.DownloadStringTaskAsync(new Uri(makeUrl(fSistMessage)));
        }

        private string makeUrl(FSistSmsMessage message)
        {
            var url = new StringBuilder();

            url.Append("http://www.fsist.com.br/sms.aspx?");
            url.Append("tel=");
            url.Append(message.PhoneNumber);
            url.Append("&msg=");
            url.Append(message.Message);

            return url.ToString();
        }

        private class FSistSmsMessage
        {
            public string Message { get; set; }
            public string PhoneNumber { get; set; }
        }
    } 
}
