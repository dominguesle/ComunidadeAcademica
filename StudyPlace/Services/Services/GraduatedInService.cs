﻿using Domain.Models;
using Domain.Repository;
using Domain.Services;
using System.Collections.Generic;

namespace Services.Services
{
    public class GraduatedInService : IGraduatedInService
    {
        private IBaseRepository<GraduatedIn> _repository;

        public GraduatedInService(IBaseRepository<GraduatedIn> repository)
        {
            _repository = repository;
        }

        public List<GraduatedIn> GetById(List<string> idsList)
        {
            var graduations = new List<GraduatedIn>();

            foreach (var idStr in idsList)
            {
                int id = 0;
                if (int.TryParse(idStr, out id))
                {
                    var obj = _repository.Get(id);
                    if (obj != null)
                        graduations.Add(obj);
                }
            }
            return graduations;
        }

        public List<GraduatedIn> GetAll()
        {
            return _repository.Get() as List<GraduatedIn>;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }  
    }
}