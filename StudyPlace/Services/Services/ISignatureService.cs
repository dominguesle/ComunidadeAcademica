﻿using System.Collections.Generic;

namespace Domain.Services
{
    public interface ISignatureService
    {
        string SignatureToken(string data, string secretKey);
        string SignatureToken(IDictionary<string, object> data, string secretKey);
    }
}
