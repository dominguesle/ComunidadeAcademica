﻿using Domain.Models;
using Domain.Repository;
using Domain.Services;
using System.Collections.Generic;
using System.Linq;

namespace Services.Services
{
    public class LanguageService : ILanguageService
    {
        private IBaseRepository<Language> _repository;

        public LanguageService(IBaseRepository<Language> repository)
        {
            _repository = repository;
        }

        public List<Language> GetById(List<string> idsList)
        {
            var languages = new List<Language>();

            foreach (var idStr in idsList)
            {
                int id = 0;
                if (int.TryParse(idStr, out id))
                {
                    var obj = _repository.Get(id);
                    if (obj != null)
                        languages.Add(obj);
                }
            }
            return languages;
        }
        public List<Language> GetAll()
        {
            return _repository.Get() as List<Language>;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

    }
}
