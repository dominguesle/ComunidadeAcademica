﻿using Domain.Models;
using Domain.Repository;
using Domain.Services;
using System.Collections.Generic;

namespace Services.Services
{
    public class OccupationAreaService : IOccupationAreaService
    {
        private IBaseRepository<OccupationArea> _repository;

        public OccupationAreaService(IBaseRepository<OccupationArea> repository)
        {
            _repository = repository;
        }

        public List<OccupationArea> GetById(List<string> idsList)
        {
            var areas = new List<OccupationArea>();

            foreach (var idStr in idsList)
            {
                int id = 0;
                if (int.TryParse(idStr, out id))
                {
                    var obj = _repository.Get(id);
                    if (obj != null)
                        areas.Add(obj);
                }
            }
            return areas;
        }        

        public List<OccupationArea> GetAll()
        {
            return _repository.Get() as List<OccupationArea>;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
