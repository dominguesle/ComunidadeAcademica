﻿using Domain.Models;
using Domain.Repository;
using Domain.Services;
using System.Collections.Generic;

namespace Services.Services
{
    public class SoftwareAndToolsService : ISoftwareAndToolsService
    {
        private IBaseRepository<SoftwareAndTool> _repository;

        public SoftwareAndToolsService(IBaseRepository<SoftwareAndTool> repository)
        {
            _repository = repository;
        }

        public List<SoftwareAndTool> GetById(List<string> idsList)
        {
            var tools = new List<SoftwareAndTool>();

            foreach (var idStr in idsList)
            {
                int id = 0;
                if (int.TryParse(idStr, out id))
                {
                    var obj = _repository.Get(id);
                    if (obj != null)
                        tools.Add(obj);
                }
            }
            return tools;
        }


        public List<SoftwareAndTool> GetAll()
        {
            return _repository.Get() as List<SoftwareAndTool>;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }      
    }
}
