﻿using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Content/js/core/jquery.min.js",
                        "~/Content/js/core/bootstrap.min.js",
                        "~/Content/js/core/jquery.slimscroll.min.js",
                        "~/Content/js/core/jquery.scrollLock.min.js",
                        "~/Content/js/core/jquery.appear.min.js",
                        "~/Content/js/core/jquery.countTo.min.js",
                        "~/Content/js/core/jquery.placeholder.min.js",
                        "~/Content/js/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/ina").Include(
                      "~/Scripts/jquery.datetimepicker.js",
                      "~/Scripts/arcnet.simple.mask.min.js",
                      "~/Scripts/jquery.mask.js",
                      "~/Scripts/jquery.autocomplete.min.js",
                      "~/Scripts/jquery.unobtrusive-ajax.min.js",
                      "~/Scripts/bootstrap-select.min.js",
                      "~/Scripts/ina.js"));

            bundles.Add(new ScriptBundle("~/bundles/appdtables").Include(             
                "~/Scripts/plugins/datatables/jquery.dataTables.min.js"));

            bundles.Add(new StyleBundle("~/Content/cssdtables").Include(
                 "~/Scripts/plugins/datatables/jquery.dataTables.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/css/bootstrap.min.css",
                        "~/Content/css/bootstrap-rating.css",
                        "~/Content/css/bootstrap-select.min.css",
                        "~/Content/css/bootstrap-select.min.css",
                        "~/Content/bootstrap-select.min.css",
                        "~/Content/css/custom.css",
                        "~/Content/Site.css",
                        "~/Content/css/jquery.autocomplete.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base").Include(
              "~/Content/themes/base/core.css",              
              "~/Content/themes/base/datepicker.css",
              "~/Content/themes/base/theme.css"));

            bundles.Add(new LessBundle("~/Content/less-front").Include("~/Content/less/main.less"));
        }
    }
}
