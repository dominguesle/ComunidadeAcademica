﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Web.App_Start
{
    public class EnvironmentConfig
    {
        private static EnvironmentConfig instance = new EnvironmentConfig();
        private string environment = "";

        private EnvironmentConfig()
        {
            var value = ConfigurationManager.AppSettings["Environment"];

            if (value != null)
                environment = value.ToString();
        }

        public static EnvironmentConfig Instance()
        {
            return instance;
        }

        public string GetKey(string key)
        {
            var value = ConfigurationManager.AppSettings[key + environment];

            return value != null ? value.ToString() : null;
        }

        public string GetEnvironment()
        {
            return environment;
        }
    }
}