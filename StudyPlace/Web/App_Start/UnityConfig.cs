using Domain.Models;
using Domain.Repository;
using Domain.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Practices.Unity;
using Services.Repository;
using Services.Services;
using System;
using System.Data.Entity;
using Web.Controllers;

namespace Web.App_Start
{
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        public static void RegisterTypes(IUnityContainer container)
        {
            // identity
            container.RegisterType<DbContext, ScaffoldContext>(
                new PerRequestLifetimeManager());
            container.RegisterType<UserManager<UserBase>>(
                new PerRequestLifetimeManager());
            container.RegisterType<SignInManager<UserBase, string>>(
                new PerRequestLifetimeManager());
            container.RegisterType<IUserStore<UserBase>, UserStore<UserBase>>(
                new PerRequestLifetimeManager());
            container.RegisterType<AuthController>(
                new InjectionConstructor());

            container.RegisterType<ISmsService, FSistSmsService>();

            // repository
            container.RegisterType(typeof(IBaseRepository<>), typeof(Repository<>));
        }
    }
}
