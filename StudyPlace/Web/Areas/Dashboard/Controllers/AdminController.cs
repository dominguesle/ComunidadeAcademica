﻿using Domain.Models;
using Domain.Repository;
using System.Linq;
using System.Web.Mvc;
using Web.Areas.Dashboard.Models;
using System.Data.Entity;
using Domain.Enum;
using AutoMapper;
using System.Collections.Generic;
using Web.Models;

namespace Web.Areas.Dashboard.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private IBaseRepository<Aluno> providerRepository;
        private IBaseRepository<Professor> leadRepository;

        public AdminController(IBaseRepository<Aluno> _providerRepository, IBaseRepository<Professor> _leadRepository)
        {
            providerRepository = _providerRepository;
            leadRepository = _leadRepository;            
        }

        public ActionResult Index()
        {
            var model = new AdminViewModel();

            model.LeadCount = leadRepository.List.Count();
            model.ProviderCount = providerRepository.List.Count();

            return View(model);
        }

        public ActionResult Leads()
        {
            var model = Mapper.Map<List<Professor>, List<ProfessorViewModel>>(leadRepository.List.ToList());

            return View(model);
        }
    }
}