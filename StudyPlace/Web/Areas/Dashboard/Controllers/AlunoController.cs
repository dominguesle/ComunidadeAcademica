﻿using AutoMapper;
using Domain.Models;
using Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Areas.Dashboard.Controllers
{
    public class AlunoController : Controller
    {
        private IBaseRepository<Aluno> alunoRepository;

        public AlunoController(IBaseRepository<Aluno> _alunoRepository)
        {
            alunoRepository = _alunoRepository;
        }

        // GET: Dashboard/Aluno
        public ActionResult Index()
        {
            var alunos = alunoRepository.List;
            
            var model = Mapper.Map<IQueryable<Aluno>, List<AlunoViewModel>>(alunos);

            return View(model);
        }

        // GET: Dashboard/Aluno/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dashboard/Aluno/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/Aluno/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Aluno/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Dashboard/Aluno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Aluno/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Dashboard/Aluno/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
