﻿using AutoMapper;
using Domain.Models;
using Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Areas.Dashboard.Controllers
{
    public class ForumController : Controller
    {
        private IBaseRepository<Aluno> alunoRepository;
        private IBaseRepository<Forum> forumRepository;
        private IBaseRepository<Mensagens> mensagemRepository;

        public ForumController(IBaseRepository<Aluno> _alunoRepository, IBaseRepository<Forum> _forumRepository,
            IBaseRepository<Mensagens> _mensagemRepository)
        {
            alunoRepository = _alunoRepository;
            forumRepository = _forumRepository;
            mensagemRepository = _mensagemRepository;
        }

        // GET: Dashboard/Aluno
        public ActionResult Index()
        {
            var foruns = forumRepository.List;
            
            var model = Mapper.Map<IQueryable<Forum>, List<ForumViewModel>>(foruns);

            return View(model);
        }

        // GET: Dashboard/Aluno/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dashboard/Aluno/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/Aluno/Create
        [HttpPost]
        public ActionResult Create(ForumViewModel model)
        {
            try
            {
                var userName = User.Identity.Name;

                var forum = Mapper.Map<ForumViewModel, Forum>(model);

                var aluno = alunoRepository.List.FirstOrDefault(x => x.UserName == userName);

                forum.AlunoCriador = aluno;
                
                forumRepository.Insert(forum);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Aluno/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Dashboard/Aluno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Aluno/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Dashboard/Aluno/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
