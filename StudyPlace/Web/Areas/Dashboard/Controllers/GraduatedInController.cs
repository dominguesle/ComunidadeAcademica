﻿using AutoMapper;
using Domain.Models;
using Domain.Repository;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Areas.Dashboard.Models.Json;
using Web.Models;

namespace Web.Areas.Dashboard.Controllers
{
    [Authorize]
    public class GraduatedInController : Controller
    {
        private readonly IBaseRepository<GraduatedIn> graduatedRepository;
        private readonly IBaseRepository<Professor> workerRepository;

        public GraduatedInController(IBaseRepository<GraduatedIn> _graduatedRepository, IBaseRepository<Professor> _workerRepository)
        {
            graduatedRepository = _graduatedRepository;
            workerRepository = _workerRepository;
        }

        [HttpPost]
        public ActionResult InsertGraduatedIn(GraduatedInViewModel model)
        {
            if (ModelState.IsValid)
            {
                var idUser = User.Identity.GetUserId();
                var professor = workerRepository.List.FirstOrDefault(x => x.Id == idUser);
                graduatedRepository.SetObjectState(professor, EntityState.Unchanged);

                var obj = new GraduatedIn();
                obj.EducationalInstitution = model.EducationalInstitution;
                obj.Course = model.Course;
                obj.Title = model.Title;
                obj.ConclusionYear = model.ConclusionYear;
                //obj.Aluno = professor;

                graduatedRepository.Insert(obj);
                
                var serializer = new JavaScriptSerializer();
                var jsonModel = Mapper.Map<GraduatedIn, GraduatedInJson>(obj);

                return Json(new { code = 200, obj = serializer.Serialize(jsonModel) });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult RemoveGraduatedIn(int id)
        {
            var obj = graduatedRepository.List.FirstOrDefault(x => x.Id == id);
            if (obj == null)
                return Json(new { message = "Registro não encontrado", code = 404 });

            graduatedRepository.Delete(obj);
            return Json(new { message = "Registro excluido com sucesso", code = 200 });
        }
    }
}