﻿using AutoMapper;
using Domain.Models;
using Domain.Repository;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Areas.Dashboard.Controllers
{
    public class MensagensController : Controller
    {
        private IBaseRepository<Aluno> alunoRepository;
        private IBaseRepository<Forum> forumRepository;
        private IBaseRepository<Mensagens> mensagemRepository;

        public MensagensController(IBaseRepository<Aluno> _alunoRepository, IBaseRepository<Forum> _forumRepository,
            IBaseRepository<Mensagens> _mensagemRepository)
        {
            alunoRepository = _alunoRepository;
            forumRepository = _forumRepository;
            mensagemRepository = _mensagemRepository;
        }

        // GET: Dashboard/Aluno
        public ActionResult Index(int id)
        {
            var mensagens = mensagemRepository.List.Where(x => x.Forum.Id == id);

            var model = Mapper.Map<IQueryable<Mensagens>, List<MensagensViewModel>>(mensagens);

            ViewBag.IdForum = id;

            return View(model);
        }

        // GET: Dashboard/Aluno/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Dashboard/Aluno/Create
        public ActionResult Create(int id)
        {
            var model = new MensagensViewModel();
            model.IdForum = id;
            return View(model);
        }

        // POST: Dashboard/Aluno/Create
        [HttpPost]
        public ActionResult Create(MensagensViewModel model, HttpPostedFileBase uploadFile)
        {
            try
            {
                var userName = User.Identity.Name;

                var mensagem = Mapper.Map<MensagensViewModel, Mensagens>(model);

                var aluno = alunoRepository.List.FirstOrDefault(x => x.UserName == userName);

                var forum = forumRepository.List.FirstOrDefault(x => x.Id == model.IdForum);

                mensagem.AlunoCriador = aluno;
                mensagem.Forum = forum;

                if (uploadFile != null)
                {
                    var uploadFileName = Guid.NewGuid().ToString() + "." + uploadFile.FileName.Split('.').Last();
                    mensagem.Anexo = AzureFileHelper.SaveFile(uploadFileName, uploadFile.InputStream);
                }

                mensagemRepository.Insert(mensagem);

                return RedirectToAction("Index", new { id = model.IdForum });
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Aluno/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Dashboard/Aluno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Dashboard/Aluno/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Dashboard/Aluno/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
