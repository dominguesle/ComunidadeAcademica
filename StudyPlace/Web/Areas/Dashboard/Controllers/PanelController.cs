﻿using AutoMapper;
using Domain.Models;
using Domain.Repository;
using Microsoft.AspNet.Identity;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web.Areas.Dashboard.Models;
using Web.Filters;
using Web.Models;

namespace Web.Areas.Dashboard.Controllers
{
    [Authorize]
    public class PanelController : Controller
    {
        private IBaseRepository<Aluno> companyRepository;
        private IBaseRepository<Professor> workerRepository;
        private IBaseRepository<Address> addressRepository;
        private IBaseRepository<Specialty> specialityRepository;      
        private IBaseRepository<Course> courseRepository;

        public PanelController(IBaseRepository<Aluno> _companyRepository, IBaseRepository<Professor> _workerRepository,
            IBaseRepository<Address> _addressRepository, IBaseRepository<Specialty> _specialityRepository,
            IBaseRepository<Course> _courseRepository)
        {
            companyRepository = _companyRepository;
            workerRepository = _workerRepository;
            addressRepository = _addressRepository;
            specialityRepository = _specialityRepository;
            courseRepository = _courseRepository;
        }

        //[ClaimsAuthorize("Role", "aluno")]
        public ActionResult IndexCompany()
        {
            var id = User.Identity.GetUserId();
            var company = companyRepository.List
                .FirstOrDefault(x => x.Id == id);

            //int workerCount = workerRepository.List
            //    .ToList().Count();

            var model = Mapper.Map<Aluno, AlunoViewModel>(company);

            if (company != null)
            {
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        //[ClaimsAuthorize("Role", "worker")]
        public ActionResult IndexWorker()
        {
            string idUser = User.Identity.GetUserId();

            var worker = workerRepository.List.FirstOrDefault(x => x.Id == idUser);

            var model = Mapper.Map<Professor, ProfessorViewModel>(worker);
            //@ViewBag.ImageUrl = worker.ImageUrl;

            return View(model);
        }

        [ClaimsAuthorize("Role", "worker")]
        public ActionResult EditCurriculum(string curriculumId)
        {
            if (curriculumId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string idUser = User.Identity.GetUserId();

            var worker = workerRepository.List.FirstOrDefault(x => x.Id == idUser);

            var model = Mapper.Map<Professor, EditWorkerViewModel>(worker);

            model.LanguageIds = (from n in model.Languages.ToList() select n.Id.ToString()).ToList();

            return View(model);
        }

        [HttpPost]
        [ClaimsAuthorize("Role", "worker")]
        public ActionResult EditCurriculum(EditWorkerViewModel model, HttpPostedFileBase profileImage)
        {
            if (ModelState.IsValid)
            {
                string idUser = User.Identity.GetUserId();

                var worker = workerRepository.List.FirstOrDefault(x => x.Id == idUser);

                var address = addressRepository.List.FirstOrDefault(x => x.Id == model.Address.Id);
                address.Street = model.Address.Street;
                address.City = model.Address.City;
                address.State = model.Address.State;

                addressRepository.Update(address);

                worker.FullName = model.FullName;
                worker.PhoneNumber = model.PhoneNumber;

                if (profileImage != null)
                {
                    var imageName = Guid.NewGuid().ToString() + "." + profileImage.FileName.Split('.').Last();
                    worker.ImageUrl = AzureFileHelper.SaveFile(imageName, profileImage.InputStream);
                }
                
                workerRepository.Update(worker);

                return RedirectToAction("IndexWorker");
            }
            
            return View(model);
        }

        public ActionResult ModalGraduate()
        {
            var courses = courseRepository.List.OrderBy(x => x.Description);
            ViewBag.Courses = new SelectList(courses, "Description", "Description");

            var titles = new List<SelectListItem> {
                new SelectListItem { Text = "Técnico", Value = "Técnico" },
                new SelectListItem { Text = "Graduação", Value = "Graduação" },
                new SelectListItem { Text = "Especialização", Value = "Especialização" },
                new SelectListItem { Text = "Mestrado", Value = "Mestrado" } ,
                new SelectListItem { Text = "Doutorado", Value = "Doutorado" }
            };

            ViewBag.Titles = new SelectList(titles, "Value", "Text");

            return PartialView("Partials/_EditGraduate");
        }

        public ActionResult ModalServiceHistory()
        {
            return PartialView("Partials/_EditServiceHistory");
        }
    }
}