﻿using System.Collections.Generic;

namespace Web.Areas.Dashboard.Models
{
    public class AdminViewModel
    {
        public int LeadCount { get; set; }

        public int ProviderCount { get; set; }

        public int PurchasedLeadCount { get; set; }

        public decimal TransactionsBalance { get; set; }

        public decimal AverageTicket { get; set; }
    }
}
