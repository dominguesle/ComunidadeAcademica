﻿using System.Collections.Generic;
using Web.Models;

namespace Web.Areas.Dashboard.Models
{
    public class CompanySearchViewModel
    {
        public string Localization { get; set; }

        public bool? TechnicalAssets { get; set; }

        public string WordExperienceKeyCompany { get; set; }

        public string HiveOfActivityId { get; set; }

        public string SpecialtyId { get; set; }
    }
}