﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Web.Models;

namespace Web.Areas.Dashboard.Models
{
    public class EditWorkerViewModel
    {
        [Key]
        public string Id { get; set; }

        [Required(ErrorMessage = "O campo Nome é obrigatório")]
        [Display(Name = "Nome Completo")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "O campo Resumo de Compentências é obrigatório")]
        [Display(Name = "Resumo de Competências")]
        public string SummaryOfSkills { get; set; }        

        [Required(ErrorMessage = "O campo Telefone é obrigatório")]
        [Display(Name = "Telefone")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "O campo Endereço é obrigatório")]
        [Display(Name = "Endereço")]
        public AddressViewModel Address { get; set; }

        public string ImageUrl { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Data")]
        public DateTime CreatedDateTime { get; set; }

        [Display(Name = "Idioma")]
        public virtual List<LanguageViewModel> Languages { get; set; }

        [Required(ErrorMessage = "Ao menos um Idioma deve ser Adicionado")]
        [Display(Name = "Idioma")]
        public List<string> LanguageIds { get; set; }

        [Display(Name = "Formação")]
        public virtual List<GraduatedInViewModel> GraduatedIn { get; set; }

        [Display(Name = "Histórico de Serviços")]
        public List<HistoricalOfServiceViewModel> HistoricalOfServices { get; set; }

        [Display(Name = "Experiência Profissional")]
        public List<ProfessionalExperienceViewModel> ProfessionalExperiences { get; set; }        
    }
}