﻿namespace Web.Areas.Dashboard.Models.Json
{
    public class GraduatedInJson
    {
        public string Id { get; set; }

        public string EducationalInstitution { get; set; }

        public string Course { get; set; }

        public string Title { get; set; }

        public int ConclusionYear { get; set; }
    }
}