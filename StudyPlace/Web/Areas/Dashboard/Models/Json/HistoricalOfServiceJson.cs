﻿using System;

namespace Web.Areas.Dashboard.Models.Json
{
    public class HistoricalOfServiceJson
    {
        public int Id { get; set; }

        public string Company { get; set; }

        public string Office { get; set; }

        public DateTime PeriodStartDate { get; set; }

        public DateTime PeriodEndDate { get; set; }

        public string SummaryOfActivities { get; set; }
    }
}