﻿using System;

namespace Web.Areas.Dashboard.Models.Json
{
    public class ProfessionalExperienceJson
    {
        public int Id { get; set; }

        public string ProjectName { get; set; }

        public string Client { get; set; }

        public DateTime StartExecutionPeriod { get; set; }

        public DateTime EndExecutionPeriod { get; set; }

        public string PerformedActivities { get; set; }

        public int ActivityAreaId { get; set; }

        public string ActivityAreaDescription { get; set; }
    }
}