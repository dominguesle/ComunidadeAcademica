﻿using System;
using Web.Models;

namespace Web.Areas.Dashboard.Models
{
    public class SaveCandidateViewModel
    {
        public int Id { get; set; }
        
        public DateTime CreatedAt { get; set; }

        public bool Active { get; set; }

        public virtual AlunoViewModel Company { get; set; }

        public virtual ProfessorViewModel Worker { get; set; }
    }
}