﻿using System.Collections.Generic;
using Web.Models;

namespace Web.Areas.Dashboard.Models
{
    public class WorkerSearchViewModel
    {
        public string Residence { get; set; }

        public string Course { get; set; }

        public string TrainingTime { get; set; }

        public string Title { get; set; }        

        public bool? TechnicalAssets { get; set; }

        public string WordExperienceKeyWorker { get; set; }        

        public string ActivityAreaId { get; set; }

        public virtual List<ProfessorViewModel> Workers { get; set; }
    }
}