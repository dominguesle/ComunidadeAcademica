﻿using AutoMapper;
using Domain.Models;
using Web.Areas.Dashboard.Models;
using Web.Areas.Dashboard.Models.Json;
using Web.Models;
using Web.Models.Json;

namespace Web.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            CreateMap<Address, AddressViewModel>();
            CreateMap<Specialty, SpecialtyViewModel>();
            CreateMap<Aluno, AlunoViewModel>();

            CreateMap<Professor, ProfessorViewModel>();
            CreateMap<GraduatedIn, GraduatedInViewModel>();
            CreateMap<Professor, EditWorkerViewModel>();
            
            CreateMap<Address, AddressJson>();
            
            CreateMap<GraduatedIn, GraduatedInJson>();
            
            CreateMap<Forum, ForumViewModel>();
            CreateMap<Mensagens, MensagensViewModel>();

        }
    }
}