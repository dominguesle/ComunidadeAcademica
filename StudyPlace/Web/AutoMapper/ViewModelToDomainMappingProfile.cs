﻿using AutoMapper;
using Domain.Models;
using Web.Areas.Dashboard.Models;
using Web.Models;

namespace Web.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            CreateMap<AddressViewModel, Address>();
            CreateMap<SpecialtyViewModel, Specialty>();
            
            CreateMap<ProfessorViewModel, Professor>();
            CreateMap<GraduatedInViewModel, GraduatedIn>();
            CreateMap<AlunoViewModel, Aluno>();
            CreateMap<EditWorkerViewModel, Professor>();

            CreateMap<ForumViewModel, Forum>();
            CreateMap<MensagensViewModel, Mensagens>();
        }
    }
}