﻿using Domain.Models;
using Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class AutoCompleteController : Controller
    {
        //IBaseRepository<Brand> brandRepository;
        //IBaseRepository<Model> modelRepository;

        //public AutoCompleteController(IBaseRepository<Brand> _brandRepository, IBaseRepository<Model> _modelRepository)
        //{
        //    brandRepository = _brandRepository;
        //    modelRepository = _modelRepository;
        //}

        public JsonResult GetCities()
        {
            return new JsonResult
            {
                Data = new { suggestions = new[] {
                    new { value = "Curitiba", data = "1" },
                    new { value = "Paranaguá", data = "2" } } },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        //public JsonResult GetBrands(string query)
        //{
        //    var brands = brandRepository.Get()
        //        .Where(b => b.Name.StartsWith(query))
        //        .Take(5)
        //        .ToList();

        //    return new JsonResult
        //    {
        //        Data = new { suggestions = brands.Select(b => new { value = b.Name, data = b.Name })},
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet
        //    };
        //}

        //public JsonResult GetModels(string id, string query)
        //{
        //    var models = modelRepository.Get()
        //        .Where(m => m.Name.StartsWith(query) && m.Brand.Name == id)
        //        .Take(5)
        //        .ToList();

        //    return new JsonResult
        //    {
        //        Data = new { suggestions = models.Select(b => new { value = b.Name, data = b.Name }) },
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet
        //    };
        //}

        public JsonResult GetVersions()
        {
            return new JsonResult
            {
                Data = new { suggestions = new[] { new { value = "1", data = "Teste" }, new { value = "2", data = "Teste 2" } } },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}