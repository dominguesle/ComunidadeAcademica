﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Domain.Models;
using Domain.Repository;
using Domain.Services;
using Newtonsoft.Json;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : IdentityBaseController
    {
        private IBaseRepository<Specialty> specialityRepository;

        public HomeController(IBaseRepository<Specialty> _specialityRepository)
        {
            specialityRepository = _specialityRepository;
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var claim = claimsIdentity.Claims.Where(c => c.Type == "Role").FirstOrDefault();

                if (claim != null && claim.Value == "admin")
                {
                    Session["Login"] = "0";
                    return RedirectToAction("Index", "Admin", new { area = "Dashboard" });
                }

                if (claim != null && claim.Value == "aluno")
                {
                    Session["Login"] = "1";
                    return RedirectToAction("IndexCompany", "Panel", new { area = "Dashboard" });
                }

                if (claim != null && claim.Value == "professor")
                {
                    Session["Login"] = "2";
                    return RedirectToAction("IndexWorker", "Panel", new { area = "Dashboard" });
                }
            }

            return View();
        }

    }
}