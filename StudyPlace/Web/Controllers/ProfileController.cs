﻿using AutoMapper;
using Domain.Models;
using Domain.Repository;
using Microsoft.AspNet.Identity;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Web.Models;
using Web.Models.Json;

namespace Web.Controllers
{
    [Authorize]
    public class ProfileController : IdentityBaseController
    {
        IBaseRepository<Aluno> companyRepository;
        IBaseRepository<Specialty> specialityRepository;
        IBaseRepository<Address> addressRepository;

        public ProfileController(IBaseRepository<Aluno> _companyRepository,
            IBaseRepository<Specialty> _specialtyRepository,
            IBaseRepository<Address> _adressRepository)
        {
            companyRepository = _companyRepository;
            specialityRepository = _specialtyRepository;
            addressRepository = _adressRepository;
        }

        public ActionResult Index()
        {
            var id = User.Identity.GetUserId();
            var company = companyRepository.List
                .FirstOrDefault(x => x.Id == id);
            if (company == null)
                return HttpNotFound();
            
            var viewModel = Mapper.Map<Aluno, EditProfileViewModel>(company);
            
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(EditProfileViewModel model, HttpPostedFileBase profileImage)
        {
            if (ModelState.IsValid)
            {
                var obj = companyRepository.List.FirstOrDefault(x => x.Id == model.Id);
                if (obj == null)
                    return HttpNotFound();

                if (profileImage != null)
                {
                    var imageName = Guid.NewGuid().ToString() + "." + profileImage.FileName.Split('.').Last();
                    obj.ImageUrl = AzureFileHelper.SaveFile(imageName, profileImage.InputStream);
                }
                
                obj.FullName = model.FullName;
                obj.PhoneNumber = model.PhoneNumber;

                companyRepository.Update(obj);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult ChangePassword(string id)
        {
            var obj = new ChangePasswordViewModel();
            obj.UserType = id;            
            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }

                if (model.UserType.Equals("1"))
                {
                    return RedirectToAction("IndexWorker", "Panel", new { area = "Dashboard", Message = "Senha alterada com sucesso." });
                }
                else
                {
                    return RedirectToAction("Index", new { Message = "Senha alterada com sucesso." });
                }
            }
            AddErrors(result);
            return View(model);
        }

        [HttpPost]
        public ActionResult InsertAddress(AddressViewModel model)
        {
            if (model == null || string.IsNullOrEmpty(model.CompanyId))
                return Json(new { message = "Endereço não pode ser Nullo", code = 404 });

            var address = Mapper.Map<AddressViewModel, Address>(model);
            address.Company = companyRepository.List.FirstOrDefault(x => x.Id == model.CompanyId);
            addressRepository.Insert(address);

            var serializer = new JavaScriptSerializer();
            var jsonModel = Mapper.Map<Address, AddressJson>(address);
            return Json(new { code = 200, obj = serializer.Serialize(jsonModel) });
        }

        public ActionResult OpenModalRegistration()
        {
            ViewBag.IdCompany = User.Identity.GetUserId();
            ViewBag.Registration = GetListRegistration();
            var model = new RegistrationOnClassCouncilViewModel();
            return PartialView("Partials/_AddRegistration", model);
        }

        public ActionResult OpenModalAddress()
        {
            ViewBag.IdCompany = User.Identity.GetUserId();
            var model = new AddressViewModel();

            return PartialView("Partials/_AddMoreAddress", model);
        }

        [HttpPost]
        public ActionResult RemoveAddress(int id)
        {
            var obj = addressRepository.List.FirstOrDefault(x => x.Id == id);
            if (obj == null)
                return Json(new { message = "Erro ao Excluir Endereço", code = 404 });

            addressRepository.Delete(obj);
            return Json(new { message = "Excluido com Sucesso", code = 200 });
        }

        private List<SelectListItem> GetListLegal()
        {
            var listLegal = new List<SelectListItem>();

            listLegal.Add(new SelectListItem { Value = "Sociedade Simples Limitada", Text = "Sociedade Simples Limitada" });
            listLegal.Add(new SelectListItem { Value = "Sociedade Anônima Aberta", Text = "Sociedade Anônima Aberta" });
            listLegal.Add(new SelectListItem { Value = "Sociedade Empresária Limitada", Text = "Sociedade Empresária Limitada" });
            listLegal.Add(new SelectListItem { Value = "Cooperativa", Text = "Cooperativa" });
            listLegal.Add(new SelectListItem { Value = "Fundação Privada", Text = "Fundação Privada" });
            listLegal.Add(new SelectListItem { Value = "Associação Privada", Text = "Associação Privada" });
            listLegal.Add(new SelectListItem { Value = "Organização da Sociedade Civil de Interesse Público (Oscip)", Text = "Organização da Sociedade Civil de Interesse Público (Oscip)" });
            listLegal.Add(new SelectListItem { Value = "Estabelecimento no Brasil de Sociedade Estrangeira", Text = "Estabelecimento no Brasil de Sociedade Estrangeira" });
            listLegal.Add(new SelectListItem { Value = "Empresa Domiciliada no Exterior", Text = "Empresa Domiciliada no Exterior" });
            listLegal.Add(new SelectListItem { Value = "Outro não especificado", Text = "Outro não especificado" });

            return listLegal;
        }

        private List<SelectListItem> GetListRegistration()
        {
            var listRegistration = new List<SelectListItem>();

            listRegistration.Add(new SelectListItem { Value = "CAU", Text = "Conselho de Arquitetura e Urbanismo" });
            listRegistration.Add(new SelectListItem { Value = "CA", Text = "Conselho de Administração" });
            listRegistration.Add(new SelectListItem { Value = "CB", Text = "Conselho de Biologia" });
            listRegistration.Add(new SelectListItem { Value = "CC", Text = "Conselho de Contabilidade" });
            listRegistration.Add(new SelectListItem { Value = "CECO", Text = "Conselho de Economia" });
            listRegistration.Add(new SelectListItem { Value = "CEA", Text = "Conselho de Engenharia e Agronomia" });
            listRegistration.Add(new SelectListItem { Value = "CEST", Text = "Conselho de Estatística" });
            listRegistration.Add(new SelectListItem { Value = "CF", Text = "Conselho de Farmácia" });
            listRegistration.Add(new SelectListItem { Value = "CMV", Text = "Conselho  de Medicina Veterinária" });
            listRegistration.Add(new SelectListItem { Value = "CQ", Text = "Conselho  de Química" });
            listRegistration.Add(new SelectListItem { Value = "CSC", Text = "Conselho de Serviço Social" });
            listRegistration.Add(new SelectListItem { Value = "OAB", Text = "Ordem dos Advogados do Brasil" });
            listRegistration.Add(new SelectListItem { Value = "unspecified", Text = "Outro não especificado" });

            return listRegistration;
        }
    }
}