﻿using AutoMapper;
using Domain.Models;
using Domain.Repository;
using Domain.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class RegisterController : IdentityBaseController
    {
        private IBaseRepository<Aluno> alunoRepository;
        private IBaseRepository<Professor> professorRepository;
        private IBaseRepository<GraduatedIn> graduatedInRepository;
        private IBaseRepository<Course> courseRepository;
        
        private IBaseRepository<Specialty> specialityRepository;
        
        public RegisterController(IBaseRepository<Professor> _professorRepository, 
            IBaseRepository<GraduatedIn> _graduatedInRepository, IBaseRepository<Course> _courseRepository,
            IBaseRepository<Specialty> _specialityRepository, IBaseRepository<Aluno> _alunoRepository)
        {
            professorRepository = _professorRepository;
            graduatedInRepository = _graduatedInRepository;
            courseRepository = _courseRepository;
            specialityRepository = _specialityRepository;
            alunoRepository = _alunoRepository;
        }

        public ActionResult Professor()
        {
            if (User.Identity.IsAuthenticated)
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var claim = claimsIdentity.Claims.Where(c => c.Type == "Role").FirstOrDefault();

                if (claim != null && claim.Value == "admin")
                {
                    Session["Login"] = "0";
                    return RedirectToAction("Index", "Admin", new { area = "Dashboard" });
                }

                if (claim != null && claim.Value == "company")
                {
                    Session["Login"] = "1";
                    return RedirectToAction("IndexCompany", "Panel", new { area = "Dashboard" });
                }

                if (claim != null && claim.Value == "worker")
                {
                    Session["Login"] = "2";
                    return RedirectToAction("IndexWorker", "Panel", new { area = "Dashboard" });
                }
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Professor(ProfessorViewModel model)
        {
            if (ModelState.IsValid)
            {
                //model. = model.Email;

                var professor = Mapper.Map<ProfessorViewModel, Professor>(model);
                professor.UserName = model.Email;
                professor.CreatedDateTime = DateTime.Now;
                professor.Claims.Add(new IdentityUserClaim
                {
                    ClaimType = "Role",
                    ClaimValue = "professor",
                    UserId = professor.Id
                });
                var passwordHash = new PasswordHasher();
                professor.PasswordHash = passwordHash.HashPassword(model.Password);
                professor.SecurityStamp = Guid.NewGuid().ToString();

                professorRepository.Insert(professor);

                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        public ActionResult ModalServiceHistory()
        {
            return PartialView("~/Views/Register/Partials/_AddServiceHistory.cshtml");
        }

        public ActionResult Aluno()
        {
            if (User.Identity.IsAuthenticated)
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                var claim = claimsIdentity.Claims.Where(c => c.Type == "Role").FirstOrDefault();

                if (claim != null && claim.Value == "admin")
                {
                    Session["Login"] = "0";
                    return RedirectToAction("Index", "Admin", new { area = "Dashboard" });
                }

                if (claim != null && claim.Value == "company")
                {
                    Session["Login"] = "1";
                    return RedirectToAction("IndexCompany", "Panel", new { area = "Dashboard" });
                }

                if (claim != null && claim.Value == "worker")
                {
                    Session["Login"] = "2";
                    return RedirectToAction("IndexWorker", "Panel", new { area = "Dashboard" });
                }
            }
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Aluno(AlunoViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserName = model.Email;

                var aluno = Mapper.Map<AlunoViewModel, Aluno>(model);
                aluno.Claims.Add(new IdentityUserClaim
                {
                    ClaimType = "Role",
                    ClaimValue = "aluno",
                    UserId = aluno.Id
                });
                var passwordHash = new PasswordHasher();
                aluno.PasswordHash = passwordHash.HashPassword(model.Password);
                aluno.SecurityStamp = Guid.NewGuid().ToString();

                alunoRepository.Insert(aluno);

                return RedirectToAction("Index", "Home");
            }
            
            return View(model);
        }

        public ActionResult Details()
        {
            return PartialView("~/Views/Home/Partials/_AddPortifolio.cshtml");
        }

        public ActionResult ModelAddress()
        {
            return PartialView("~/Views/Home/Partials/_AddMoreAddress.cshtml");
        }

        #region Métodos Privados para Tratamento dos Dados Apresentados na Interface

        private Aluno SetCompany(AlunoViewModel model)
        {
            var company = new Aluno();
            company.Email = model.Email;
            company.FullName = model.FullName;
            company.AddClaimsToUser();
            company.SetUserName();

            return company;
        }

        //private List<Address> SetAddresses(List<AddressViewModel> addresses)
        //{
        //    return Mapper.Map<List<AddressViewModel>, List<Address>>(addresses);
        //}
        
        private Professor SetWorker(ProfessorViewModel model)
        {
            var worker = new Professor();
            worker.FullName = model.FullName;
            worker.Email = model.Email;
            worker.AddClaimsToUser();
            worker.SetUserName();

            return worker;
        }

        private Address GetAddress(AddressViewModel address)
        {
            return Mapper.Map<AddressViewModel, Address>(address);
        }

        private List<GraduatedIn> GetGraduatedIn(List<GraduatedInViewModel> graduate)
        {
            return Mapper.Map<List<GraduatedInViewModel>, List<GraduatedIn>>(graduate);
        }
    
        #endregion

    }
}