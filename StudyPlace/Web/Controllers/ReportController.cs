﻿using Domain.Models;
using Domain.Repository;
using Services.Helpers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class ReportController : Controller
    {
        IBaseRepository<Professor> clientRepository;
        IBaseRepository<Aluno> companyRepository;

        public ReportController(IBaseRepository<Professor> _clientRepository, IBaseRepository<Aluno> companyService)
        {
            clientRepository = _clientRepository;
            companyRepository = companyService;
        }

        public ActionResult Clientes()
        {
            var data = clientRepository
                .List
                .Select(c => new
                {
                    c.Email,
                    Nome = c.FullName,
                    Telefone = c.PhoneNumber,
                    //Região = c.City,
                    Data = c.CreatedDateTime
                })
                .ToList()
                .Select(c => new
                {
                    c.Nome,
                    c.Email,
                    c.Telefone,
                    //c.Região,
                    Data = c.Data.Subtract(new TimeSpan(3,0,0))
                })
                .OrderByDescending(c => c.Data)
                .CopyGenericToDataTable();

            var exported = ExcelExporterHelper.Export(data);

            Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"Relatorio de Clientes - {0:dd/MM/yyyy}.xlsx\"", DateTime.Today));
            return File(exported, "application/vnd.ms-excel");
        }

        public ActionResult Fornecedores()
        {
            var data = companyRepository
                .List
                .Select(c => new
                {
                    c.Email,
                    Nome_Contato = c.FullName,
                    Telefone = c.PhoneNumber,
                    Data = c.CreatedDateTime
                })
                .ToList()
                .Select(c => new
                {
                    c.Nome_Contato,
                    c.Email,
                    c.Telefone,
                    Data = c.Data.Subtract(new TimeSpan(3, 0, 0))
                })
                .OrderByDescending(c => c.Data)
                .CopyGenericToDataTable();


            var exported = ExcelExporterHelper.Export(data);

            Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"Relatorio de Fornecedores - {0:dd/MM/yyyy}.xlsx\"", DateTime.Today));
            return File(exported, "application/vnd.ms-excel");
        }

        public ActionResult FornecedoresExt()
        {
            var data = companyRepository
                .List
                .Select(c => new
                {
                    c.Email,
                    Nome_Contato = c.FullName,
                    Telefone = c.PhoneNumber,
                    Data = c.CreatedDateTime
                })
                .ToList()
                .Select(c => new
                {
                    c.Nome_Contato,
                    c.Email,
                    c.Telefone,
                   // c.Região,
                    Data = c.Data.Subtract(new TimeSpan(3, 0, 0))
                })
                .OrderByDescending(c => c.Data)
                .CopyGenericToDataTable();

            var exported = ExcelExporterHelper.Export(data);

            Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"Relatorio de Fornecedores - {0:dd/MM/yyyy}.xlsx\"", DateTime.Today));
            return File(exported, "application/vnd.ms-excel");
        }
    }
}