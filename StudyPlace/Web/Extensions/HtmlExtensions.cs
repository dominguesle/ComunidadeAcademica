﻿using System;
using System.Web.Mvc;

namespace Web.Extensions
{
    public static class HtmlExtensions
    {
        public static bool IsCurrentControllerAndAction(this HtmlHelper html, string actionName, string controllerName)
        {
            bool result = false;
            string normalizedControllerName = controllerName.EndsWith("Controller") ? controllerName : String.Format("{0}Controller", controllerName);

            var viewContext = html.ViewContext;

            if (viewContext == null) return false;
            if (String.IsNullOrEmpty(actionName)) return false;

            if (viewContext.Controller.GetType().Name.Equals(normalizedControllerName, StringComparison.InvariantCultureIgnoreCase) &&
                viewContext.Controller.ValueProvider.GetValue("action").AttemptedValue.Equals(actionName, StringComparison.InvariantCultureIgnoreCase))
            {
                result = true;
            }

            return result;
        }

        public static string IsActiveControllerAndAction(this HtmlHelper html, string actionName, string controllerName)
        {
            string result = "";
            string normalizedControllerName = controllerName.EndsWith("Controller") ? controllerName : String.Format("{0}Controller", controllerName);

            var viewContext = html.ViewContext;

            if (viewContext == null) return string.Empty;
            if (String.IsNullOrEmpty(actionName)) return string.Empty;

            if (viewContext.Controller.GetType().Name.Equals(normalizedControllerName, StringComparison.InvariantCultureIgnoreCase) &&
                viewContext.Controller.ValueProvider.GetValue("action").AttemptedValue.Equals(actionName, StringComparison.InvariantCultureIgnoreCase))
            {
                result = "active";
            }

            return result;
        }

        public static string IsActiveController(this HtmlHelper html, string controllerName)
        {
            string result = "";
            string normalizedControllerName = controllerName.EndsWith("Controller") ? controllerName : String.Format("{0}Controller", controllerName);

            var viewContext = html.ViewContext;

            if (viewContext == null) return string.Empty;
            
            if (viewContext.Controller.GetType().Name.Equals(normalizedControllerName, StringComparison.InvariantCultureIgnoreCase))
            {
                result = "active";
            }

            return result;
        }
    }
}
