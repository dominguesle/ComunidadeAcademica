﻿using Domain.Services;
using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web.App_Start;
using Microsoft.Practices.Unity;
using Web.AutoMapper;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = false;
            AutoMapperConfig.RegisterMappers();

            //var robot = UnityConfig.GetConfiguredContainer().Resolve<IRobot>();
            //robot.Run();
        }

        private void Application_BeginRequest()
        {
            if ((Request.Url.Host.Contains("autoexato.com.br") || Request.Url.Host.Contains("carroexato.com.br") ||
                Request.Url.Host.Contains("autoexato.com") || Request.Url.Host.Contains("carroexato.com")) && 
                !Request.Url.Host.StartsWith("www") && !Request.Url.IsLoopback && !Request.Url.Host.Contains("dev"))
            {
                var builder = new UriBuilder("http://www.autoexato.com");
                builder.Host = "www.autoexato.com";
                builder.Scheme = "http";
                Response.StatusCode = 301;
                Response.AddHeader("Location", builder.ToString());
                Response.End();
            }
        }
    }
}
