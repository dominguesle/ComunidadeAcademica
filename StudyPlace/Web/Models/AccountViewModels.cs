﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "O campo E-mail deve ser preenchido")]
        [EmailAddress(ErrorMessage = "O E-mail informado não é válido")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O campo Senha deve ser preenchido")]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }
        public bool IsModal { get; set; }
    }

    public class RegisterViewModel
    {
        [Key]
        public string Id { get; set; }

        [Required(ErrorMessage = "Resumo das Competências é um campo obrigatório")]
        [Display(Name = "Resumo das Competências")]
        public string ResumoCompetencias { get; set; }

        [Required(ErrorMessage = "Nome Completo é um campo obrigatório")]
        [Display(Name = "Nome Completo")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "E-mail é um campo obrigatório")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Telefone")]
        public string Phone { get; set; }

        [Required]
        public virtual AddressViewModel Address { get; set; }

        [Required(ErrorMessage = "Formação é um campo obrigatório")]
        [Display(Name = "Formação")]
        public List<string> GraduatedInList { get; set; }

        [Required(ErrorMessage = "Idiomas é um campo obrigatório")]
        [Display(Name = "Idiomas")]
        public List<string> LanguagesList { get; set; }

        [Required(ErrorMessage = "Histórico de Serviços é um campo obrigatório")]
        [Display(Name = "Histórico de Serviços")]
        public List<string> HistoricoServicos { get; set; }

        [Required(ErrorMessage = "Experiência Profissional é um campo obrigatório")]
        [Display(Name = "Experiência Profissional")]
        public List<string> PrifessionalExperienceList { get; set; }

        [Required]
        [Display(Name = "Usuário")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "A {0} deve possuir pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar senha")]
        [Compare("Password", ErrorMessage = "As senhas não conferem.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "A {0} deve possuir pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar senha")]
        [Compare("Password", ErrorMessage = "As senhas não conferem.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ConfirmationAccount
    {
        public string Email { get; set; }

        public int LinkConfirmation
        {
            get; set;
        }
    }

}