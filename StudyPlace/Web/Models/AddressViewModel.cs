﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class AddressViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo Rua deve ser preenchido")]
        [Display(Name = "Rua")]
        public string Street { get; set; }

        //[Required(ErrorMessage = "O campo Número deve ser preenchido")]
        [Display(Name = "Número")]
        public string Number { get; set; }

        //[Required(ErrorMessage = "O campo Complemento deve ser preenchido")]
        [Display(Name = "Complemento")]
        public string Complement { get; set; }

        //[Required(ErrorMessage = "O campo CEP deve ser preenchido")]
        [Display(Name = "CEP")]
        public string CEP { get; set; }

        //[Required(ErrorMessage = "O campo Bairro deve ser preenchido")]
        [Display(Name = "Bairro")]
        public string Neighborhood { get; set; }

        [Required(ErrorMessage = "O campo Cidade deve ser preenchido")]
        [Display(Name = "Cidade")]
        public string City { get; set; }

        [Required(ErrorMessage = "O campo Estado deve ser preenchido")]
        [Display(Name = "Estado")]
        public string State { get; set; }

        //[Required(ErrorMessage = "O campo Filial deve ser preenchido")]
        [Display(Name = "Filial")]
        public bool FilialAddress { get; set; }

        public string CompanyId { get; set; }
        
    }
}