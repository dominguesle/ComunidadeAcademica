﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Web.Areas.Dashboard.Models;
using Web.Models.Enuns;

namespace Web.Models
{
    public class AlunoViewModel
    {
        public AlunoViewModel()
        {
        }
        
        [Key]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Nome Completo é um campo obrigatório")]
        [Display(Name = "Nome")]
        public string FullName { get; set; }
        
        [Required(ErrorMessage = "E-mail é um campo obrigatório")]
        [Display(Name = "E-mail")]
        [EmailAddress]
        public string Email { get; set; }
        
        public string ImageUrl { get; set; }

        public string UserName { get; set; }

        [Required(ErrorMessage = "Curso é um campo obrigatório")]
        [Display(Name = "Curso")]
        public string Curso { get; set; }

        [Required(ErrorMessage = "Instituição é um campo obrigatório")]
        [Display(Name = "Instituição")]
        public string Instituicao { get; set; }

        //public virtual List<AddressViewModel> Addresses { get; set; }        


        //[Display(Name = "Especialidade(s)")]
        //public virtual List<SpecialtyViewModel> Specialties { get; set; }

        [Required(ErrorMessage = "Senha e um campo obrigatório")]
        [StringLength(100, ErrorMessage = "A {0} deve possuir pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar senha")]
        [Compare("Password", ErrorMessage = "As senhas não conferem.")]
        public string ConfirmPassword { get; set; }
    }
}