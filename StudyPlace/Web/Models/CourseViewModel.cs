﻿namespace Web.Models
{
    public class CourseViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}