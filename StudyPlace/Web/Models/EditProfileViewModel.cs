﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class EditProfileViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "O Campo Razão Social é um campo obrigatório")]        
        [Display(Name = "Razão Social")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "O Campo CNPJ é um campo obrigatório")]
        [Display(Name = "CNPJ")]
        public string CNPJ { get; set; }

        [Required(ErrorMessage = "O Campo Responsável é um campo obrigatório")]
        [Display(Name = "Responsável")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "O Campo E-mail é um campo obrigatório")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O Campo Resumo das Competências é um campo obrigatório")]
        [Display(Name = "Resumo das Competências")]
        public string SummaryOfSkills { get; set; }

        [Required(ErrorMessage = "O Campo Telefone é um campo obrigatório")]
        [Display(Name = "Telefone")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "O Campo Natureza Jurídica é um campo obrigatório")]
        [Display(Name = "Natureza Jurídica")]
        public string Legal { get; set; }
        
        [Display(Name = "Capital Social")]
        public string ShareCapital { get; set; }

        [Required(ErrorMessage = "Especialidade(s) é um campo obrigatório")]
        [Display(Name = "Especialidade(s)")]
        public List<string> SpecialtiesIds { get; set; }

        [Display(Name = "Especialidade(s)")]
        public virtual List<SpecialtyViewModel> Specialties { get; set; }

        [Display(Name = "Área(s) de Atividade")]
        [Required(ErrorMessage = "O Campo Área(s) de Atividade é um campo obrigatório")]
        public List<string> HiveOfActivitiesIds { get; set; }

        [Display(Name = "Área(s) de Atividade")]
        public virtual List<HiveOfActivityViewModel> HiveOfActivities { get; set; }

        [Display(Name = "Registro(s) em Conselho(s) de Classe")]
        public virtual List<RegistrationOnClassCouncilViewModel> RegistrationBoardOfClasses { get; set; }
       
        [Display(Name = "Endereços")]
        public virtual List<AddressViewModel> Addresses { get; set; }
       
        [Display(Name = "Portfólio")]
        public virtual List<PortfolioViewModel> Portfolios { get; set; }

        public string ImageUrl { get; set; }
    }
}