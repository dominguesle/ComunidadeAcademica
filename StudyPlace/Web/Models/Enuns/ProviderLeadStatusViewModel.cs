﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.Enuns
{
    public enum ProviderLeadStatusViewModel
    {
        [Display(Name = "Não Especificado")]
        NaoEspecificado,

        [Display(Name = "Em Aberto")]
        EmAberto,

        [Display(Name = "Contato Realizado")]
        ContatoRealizado,

        [Display(Name = "Negócio Fechado")]
        NegocioFechado
    }
}