﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Web.Models.Enuns
{
    public enum ProviderPlanViewModel
    {
        [Description("Não Especificado")]
        [Display(Name = "Não Especificado")]
        None,

        [Description("Básico")]
        [Display(Name = "Básico")]
        Basic,

        [Description("Intermediário")]
        [Display(Name = "Intermediário")]
        Intermediate,

        [Description("Avançado")]
        [Display(Name = "Avançado")]
        Pro
    }
}