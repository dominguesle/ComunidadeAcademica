﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Web.Areas.Dashboard.Models;
using Web.Models.Enuns;

namespace Web.Models
{
    public class ForumViewModel
    {
        public ForumViewModel()
        {
            this.CreateDate = DateTime.UtcNow;
        }
        
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nome do Forum é um campo obrigatório")]
        [Display(Name = "Nome do Forum")]
        public string NomeForum { get; set; }
        
        [Required(ErrorMessage = "Curso Referência é um campo obrigatório")]
        [Display(Name = "Curso Referência")]
        public string Curso { get; set; }

        public AlunoViewModel AlunoCriador { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; }
    }
}