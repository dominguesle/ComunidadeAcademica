﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class GraduatedInViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo Curso deve ser preenchido")]
        [Display(Name = "Curso")]
        public string Course { get; set; }

        [Required(ErrorMessage = "Campo Título deve ser preenchido")]
        [Display(Name = "Título")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Campo Ano de Conclusão deve ser preenchido")]
        [Display(Name = "Ano de Conclusão")]
        public int ConclusionYear { get; set; }

        [Required(ErrorMessage = "Campo Instituição de Ension deve ser preenchido")]
        [Display(Name = "Insituição de Ensino")]
        public string EducationalInstitution { get; set; }

        public string OtherCourse { get; set; }

        public string WorkerId { get; set; }

        public CourseViewModel CourseModel { get; set; }

        public List<ProfessorViewModel> Workers { get; set; }
    }
}