﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class HistoricalOfServiceViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Empresa")]
        public string Company { get; set; }

        [Required]
        [Display(Name = "Cargo/Função")]
        public string Office { get; set; }

        [Required]
        [Display(Name = "Data da admissão")]
        public DateTime PeriodStartDate { get; set; }

        [Required]
        [Display(Name = "Data da saída")]
        public DateTime PeriodEndDate { get; set; }

        [Display(Name = "Resumo das Atividades")]
        public string SummaryOfActivities { get; set; }

       public virtual ProfessorViewModel Worker { get; set; }
    }
}