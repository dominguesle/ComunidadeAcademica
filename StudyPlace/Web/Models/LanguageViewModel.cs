﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class LanguageViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo descrição deve ser preenchido")]
        [MaxLength(100)]        
        public string Description { get; set; }

        public List<ProfessorViewModel> Workers { get; set; }
    }
}