﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Web.Areas.Dashboard.Models;
using Web.Models.Enuns;

namespace Web.Models
{
    public class MensagensViewModel
    {
        public MensagensViewModel()
        {
            this.CreateDate = DateTime.UtcNow;
            this.Forum = new ForumViewModel();
            this.AlunoCriador = new AlunoViewModel();
        }
        
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Mensagem é um campo obrigatório")]
        [Display(Name = "Mensagem")]
        public string Mensagem { get; set; }
        
        [Display(Name = "Anexo")]
        public string Anexo { get; set; }

        public int IdForum { get; set; }

        public ForumViewModel Forum { get; set; }

        public AlunoViewModel AlunoCriador { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; }
    }
}