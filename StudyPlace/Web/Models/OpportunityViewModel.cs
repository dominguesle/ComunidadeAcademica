﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Web.Models
{
    public class OpportunityViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo Data de Publicação é obrigatório")]
        [Display(Name = "Data de Publicação")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime PublicationDate { get; set; }

        [Required(ErrorMessage = "O campo Orgão Licitante é obrigatório")]
        [Display(Name = "Orgão Licitante")]
        public string OrganBidder { get; set; }

        [Required(ErrorMessage = "O campo Tipo de Contratação é obrigatório")]
        [Display(Name = "Tipo de Contratação")]
        public string TypeOfContract { get; set; }

        [Required(ErrorMessage = "O campo Local é obrigatório")]
        [Display(Name = "Localidade")]
        public string Local { get; set; }

        [Display(Name = "Objeto")]
        public string Attachments { get; set; }

        [Required(ErrorMessage = "O campo Valor é obrigatório")]
        [Display(Name = "Valor")]
        public decimal Value { get; set; }

        public string ValueInReal
        {
            get
            {
                CultureInfo cultureInfo = CultureInfo.GetCultureInfo("pt-BR");
                return String.Format(cultureInfo, "{0:C}", Value);
            }
        }

        [Required(ErrorMessage = "O campo Tipo de Parceria é obrigatório")]
        [Display(Name = "Tipo de Parceria")]
        public string PartnerType { get; set; }

        [Display(Name = "Percentual/Valor Proposto")]
        public decimal Percentual { get; set; }

        [Required(ErrorMessage = "O campo Experiência Requerida é um campo obrigatório")]
        [Display(Name = "Experiência Requerida")]
        public string ExperienceRequired { get; set; }

        [Required(ErrorMessage = "O campo Especialidade é um campo obrigatório")]
        [Display(Name = "Especialidade")]
        public string SpecialtyId { get; set; }

        public virtual SpecialtyViewModel Specialty { get; set; }

        [Required(ErrorMessage = "O campo Especialidade é um campo obrigatório")]
        [Display(Name = "Área de Atividade")]
        public string ActivityAreaId { get; set; }

        public virtual ActivityAreaViewModel ActivityArea { get; set; }

        [Display(Name = "Preço do Lead")]
        public decimal Price { get; set; }

        
    }
}