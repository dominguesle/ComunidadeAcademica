﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class PortfolioViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo Nome do Projeto é obrigatório")]
        [Display(Name = "Nome do Projeto")]
        public string ProjectName { get; set; }

        [Required(ErrorMessage = "O campo Cliente é obrigatório")]
        [Display(Name = "Cliente")]
        public string Client { get; set; }

        [Required(ErrorMessage = "O campo Data de Início do Período de Execução é obrigatório")]
        [Display(Name = "Data de Início do Período de Execução")]
        public DateTime StartExecutionPeriod { get; set; }

        [Required(ErrorMessage = "O campo Data Final do Período de Execução é obrigatório")]
        [Display(Name = "Data Final do Período de Execução")]
        public DateTime EndExecutionPeriod { get; set; }

        [Required(ErrorMessage = "O campo Área de Atividade é obrigatório")]
        [Display(Name = "Área de Atividade")]
        public virtual ActivityAreaViewModel ActivityArea { get; set; }

        public int ActivityAreaId { get; set; }
        
        [Required(ErrorMessage = "O campo Descrição do Trabalho e Atividades Desempenhadas é obrigatório")]
        [Display(Name = "Descrição do Trabalho e Atividades Desempenhadas")]
        public string DescriptionAcitviesAndWork { get; set; }
        
        [Display(Name = "Reponsável pelas Informações")]
        public string ResponsibleForInformation { get; set; }

        public string CompanyId { get; set; }

        // Novos Campos
        [Display(Name ="Localidade")]
        public string Locale { get; set; }

        [Display(Name = "Valor Aproximado")]
        public decimal? RangeValue { get; set; }

        [Display(Name = "Possui Atestado Técnico?")]
        public bool? HasTechnicalCertificate { get; set; }
    }
}