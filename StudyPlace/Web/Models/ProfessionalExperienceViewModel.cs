﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class ProfessionalExperienceViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nome do Projeto")]
        public string ProjectName { get; set; }

        [Required]
        [Display(Name = "Cargo / Função")]
        public string Client { get; set; }

        [Required]
        [Display(Name = "Início das Atividades")]
        public DateTime StartExecutionPeriod { get; set; }

        [Required]
        [Display(Name = "Término das Atividades")]
        public DateTime EndExecutionPeriod { get; set; }

        [Required]
        [Display(Name = "Atividades Desempenhadas")]
        public string PerformedActivities { get; set; }

        public virtual ProfessorViewModel Worker { get; set; }
               
        [Display(Name = "Área de Atividade")]
        public ActivityAreaViewModel ActivityArea { get; set; }

        [Display(Name = "Área de Atividade")]
        [Required]
        public string ActivityAreaDescription { get; set; }

        // Novos Campos
        public int ProfessionalSpecialtyId { get; set; }

        public virtual List<ProfessionalSpecialtyViewModel> ProfessionalSpecialties { get; set; }

        public bool? HasTechnicalCertificate { get; set; }
    }
}