﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class ProfessionalSpecialtyViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Category { get; set; }

        [Required(ErrorMessage = "Campo descrição deve ser preenchido")]
        [MaxLength(1000)]
        public string Description { get; set; }

        public List<ProfessionalExperienceViewModel> ProfessionalExperiences { get; set; }
    }
}