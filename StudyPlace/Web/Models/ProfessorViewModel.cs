﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Web.Areas.Dashboard.Models;

namespace Web.Models
{
    public class ProfessorViewModel
    {
        [Key]
        public int UserId { get; set; }

        [Required(ErrorMessage = "O campo Nome é obrigatório")]
        [Display(Name = "Nome Completo")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "O campo E-mail é obrigatório")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O campo Curso Principal é obrigatório")]
        [Display(Name = "Curso Principal")]
        public string PrincipalCourse { get; set; }

        [Required(ErrorMessage = "O campo Instituição Principal é obrigatório")]
        [Display(Name = "Instituição Principal")]
        public string Instituicao { get; set; }

        [Display(Name = "Formação")]
        public virtual List<GraduatedInViewModel> GraduatedIn { get; set; }

        public List<string> GraduatedIds { get; set; }

        public string UserName { get; set; }

        [Display(Name = "Data" )]
        public DateTime CreatedDateTime { get; set; }

        [Required(ErrorMessage = "Senha e um campo obrigatório")]
        [StringLength(100, ErrorMessage = "A {0} deve possuir pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar senha")]
        [Compare("Password", ErrorMessage = "As senhas não conferem.")]
        public string ConfirmPassword { get; set; }

        
    }
}