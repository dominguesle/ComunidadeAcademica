﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ProviderLeadViewModel
    {
        public Domain.Models.Aluno Provider { get; set; }
        public IQueryable<Domain.Models.Professor> LeadList { get; set; }
        public Domain.Models.Professor Lead { get; set; }
        public bool IsBalanceEnough { get; set; }
    }
}