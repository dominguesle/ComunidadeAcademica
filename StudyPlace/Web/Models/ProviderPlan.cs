﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public static class ProviderPlan
    {
        public static decimal GetPrice(int id)
        {
            Domain.Enum.ProviderPlan providerPlan = (Domain.Enum.ProviderPlan)id;

            if (providerPlan.Equals(Domain.Enum.ProviderPlan.Basic))
                return 49.90m;
            if (providerPlan.Equals(Domain.Enum.ProviderPlan.Intermediate))
                return 179.90m;
            if (providerPlan.Equals(Domain.Enum.ProviderPlan.Pro))
                return 349.90m;

            return 0m;
        }
    }
}