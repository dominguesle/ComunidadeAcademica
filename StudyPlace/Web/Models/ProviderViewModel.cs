﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ProviderViewModel
    {
        public Domain.Models.Aluno Provider { get; set; }
        public int Leads { get; set; }
        public int PurchasedLeads { get; set; }
        public decimal PercentageLeads { get; set; }
    }
}