﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class RegistrationOnClassCouncilViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo Conselho é obrigatório")]
        [Display(Name = "Conselho")]
        public string Council { get; set; }

        [Required(ErrorMessage = "O campo Registro é obrigatório")]
        [Display(Name = "Registro")]
        public string Registration { get; set; }

        public string CompanyId { get; set; }

        public virtual AlunoViewModel Company { get; set; }

        public string WorkerId { get; set; }

        public virtual ProfessorViewModel Worker { get; set; }
    }
}