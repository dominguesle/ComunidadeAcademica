﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class SubscriptionViewModel
    {
        [Key]
        public int Id { get; set; }

        public ProviderViewModel Provider { get; set; }

        public Domain.Enum.ProviderPlan ProviderPlan { get; set; }

        [Required]
        public SubscriptionViewModel ParentSubscription { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        [Required]
        public DateTime? NextRenovationDate { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public bool IsCanceled { get; set; }

        [Required]
        public bool IsRenewed { get; set; }       
    }
}