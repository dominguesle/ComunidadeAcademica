﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class UserBaseViewModel
    {
        public string FullName { get; set; }

        public string confirmationLink { get; set; }
    }
}