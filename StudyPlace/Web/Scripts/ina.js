﻿$(function () {
    $('.ina-pagseguro-subscription-button').click(function () {
        var el = $(this);

        var description = el.attr('data-description') || "";
        var price = el.attr('data-price') || 0;
        var quantity = el.attr('data-quantity') || 1;

        var successCallback = el.attr('data-success-callback');
        var errorCallback = el.attr('data-error-callback');

        $.ajax({
            type: 'GET',
            url: '/pagSeguro/getPreApprovalCode?description=' + description + '&price=' + price + '&quantity=' + quantity,
            success: function (data) {
                PagSeguroLightbox({ code: data.PagSeguroCode }, { success: window[successCallback], abort: window[errorCallback] });
            }
        });
    });

    $('.ina-pagseguro-transaction-button').click(function () {
        var el = $(this);

        var description = el.attr('data-description') || "";
        var price = el.attr('data-price') || 0;
        var quantity = el.attr('data-quantity') || 1;

        var successCallback = el.attr('data-success-callback');
        var errorCallback = el.attr('data-error-callback');

        $.ajax({
            type: 'GET',
            url: '/pagSeguro/getTransactionCode?description=' + description + '&price=' + price + '&quantity=' + quantity,
            success: function (data) {
                PagSeguroLightbox({ code: data.PagSeguroCode }, { success: window[successCallback], abort: window[errorCallback] });
            }
        });
    });

    // INA DatetimePicker
    $(".ina-datetimepicker").datetimepicker({ lang: 'pt-BR', format: 'd/m/Y H:i' });
    $(".ina-datepicker").datetimepicker({ lang: 'pt-BR', format: 'd/m/Y', timepicker: false });
    $(".ina-timepicker").datetimepicker({ lang: 'pt-BR', format: 'H:i', datepicker: false });
    $(".ina-datetimepicker-mask").datetimepicker({ lang: 'pt-BR', format: 'd/m/Y H:i', mask: true });
    $(".ina-datepicker-mask").datetimepicker({ lang: 'pt-BR', format: 'd/m/Y', timepicker: false, mask: true });
    $(".ina-timepicker-mask").datetimepicker({ lang: 'pt-BR', format: 'H:i', datepicker: false, mask: true });

    $('.ina-cnpj-mask').arcnetmask('cnpj');
    $('.ina-cpf-mask').arcnetmask('cpf');
    $('.ina-date-mask').arcnetmask('data');
    $('.ina-time-mask').arcnetmask('hora');
    $('.ina-datetime-mask').arcnetmask('data_hora');
    $('.ina-cep-mask').arcnetmask('cep');
    $('.ina-money-mask').arcnetmask('dinheiro3');

    $('.ina-only-numbers').arcnetmask("#", { reverse: true, maxlength: false });

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('.ina-phone-mask').mask(SPMaskBehavior, spOptions);



    $('.ina-autocomplete').each(function () {
        var el = $(this);
        var url = el.attr('data-url');
        var callback = el.attr('data-callback');
        el.autocomplete({
            serviceUrl: url,
            onSelect: window[callback],
            minChars: 1
        });
    });
});